from docx import Document
from docx.shared import Inches
from datetime import datetime, timedelta
import os, os.path
import sys

GIT_PATH = ".."

def parse_raw(raw_filename):
    with open(raw_filename,'r',encoding="utf8") as f:
        lines = f.readlines()
        return [line.strip("*\n\r") for line in lines]

def main():

    if len(sys.argv) < 3:
        print("Usage: make_summary \"name of raw call file\"")
        sys.exit()

    raw_file = sys.argv[1] # Raw summary file

    now = datetime.now()+timedelta(days=int(sys.argv[2])) # current date and time

    date_time = now.strftime("%d-%m-%Y")

    # Find meeting number    
    files = os.listdir(GIT_PATH)
    meeting_number = len([f for f in files if f.find(".docx")!=-1])

    # Create document
    document_name = f"MeetingNotice_{meeting_number}_{date_time}.docx"
    document = Document()

    # Title
    document.add_heading(f"Meeting notice #{meeting_number}", 0)

    # Introduction
    p = document.add_paragraph(f"This document contains the meeting noice of meeting {meeting_number}.")
    p = document.add_paragraph(f"The meeting will be held at {date_time} in the groups project room which is at Helsingforsgade 27 - room 245.")

    # Summary title
    document.add_heading('Meeting points', level=1)

    # Generate list of summary elements
    for sum_item in parse_raw(raw_file):
        if('\t' in sum_item):
            sum_item = sum_item.strip("\t")
            paragraph = document.add_paragraph(sum_item,style='List Bullet')
            paragraph_format = paragraph.paragraph_format
            paragraph_format.left_indent = Inches(0.5)
        else:
            paragraph = document.add_paragraph(sum_item,style='List Bullet')
        
    
    p = document.add_paragraph(f"Any files needed for the meeting will be provided in a .zip file which will be attached to the mail.")

    
    document.save(GIT_PATH+"\\"+document_name)

if __name__ == "__main__":
    main()