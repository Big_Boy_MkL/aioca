from docx import Document
from datetime import datetime
import os, os.path
import sys

GIT_PATH = ".."

def parse_raw(raw_filename):
    with open(raw_filename,'r',encoding="utf8") as f:
        lines = f.readlines()
        return [line.strip("*\n\r") for line in lines]

def main():

    if len(sys.argv) < 2:
        print("Usage: make_summary \"name of raw summary file\"")
        sys.exit()

    raw_file = sys.argv[1] # Raw summary file

    now = datetime.now() # current date and time
    date_time = now.strftime("%m-%d-%Y")

    # Find meeting number    
    files = os.listdir(GIT_PATH)
    meeting_number = len([f for f in files if f.find(".docx")!=-1])

    # Create document
    document_name = f"Summary_{meeting_number}_{date_time}.docx"
    document = Document()

    # Title
    document.add_heading(f"Meeting #{meeting_number} summary", 0)

    # Introduction
    p = document.add_paragraph(f"This document contains the summary of meeting {meeting_number}. The meeting was held at {date_time}")
    p = document.add_paragraph(f"Attendees: ")
    p.add_run('Michael, Jesper, Mikkel og Nicklas').bold = True

    # Summary title
    document.add_heading('Summary', level=1)

    # Generate list of summary elements
    for sum_item in parse_raw(raw_file):
        document.add_paragraph(sum_item, style='List Bullet')

    document.save(GIT_PATH+"\\"+document_name)

if __name__ == "__main__":
    main()