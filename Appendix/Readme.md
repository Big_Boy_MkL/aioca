# Appendix for AIOCA
The structure for the attachments handed in with the Bachelor journal.
This document follows the structure shown below.

* Folder
	- Document / Folder
		- Document

---

* Analysis
	- AuthenticationArchitecture
	- Connectivity
	- Hashing
	- MainController
	- SupportController
	- WiFiChip
* Architecture
	- Architecture Paper
* Datasheets
	- Datasheets used in design process for MainCtrl, SupportCtrl and WiFiChip
* Design
	- MainCtrl
		- Frontcam
		- General utilities
		- JPEG
		- SD Card
		- SPI Protocol
		- Test Utilities
	- SmartphoneApp
		- Camera
		- GUI
		- Login
		- MainActivity
		- Networking
		- SavedVideo
		- Service
		- Settings
	- SupportCtrl
		- SoftwareDesign
	- WiFiChip
		- SoftwareDesign
	- SharedHardware
* Implementation
	- Doxygen
		- MainController Doxygen
		- Smartphone Application Javadocs
		- Support Controller Doxygen
		- WiFiChip Doxygen
	- Source code
		- MainController source
		- Smartphone Application source
		- SupportController source
		- WiFiChip source
* Logs
	- SCRUMTaskLogs
		- Sprint[1..9]
	- GitCommitLogs
		- aioca_app_dev logs (smartphone application)
		- aioca_dev logs (mainctrl, supportctrl, wifichip)
		- aioca logs (shared documents)
* Meetings
	- MeetingCalls
		- MeetingNotices[1..11]
	- MeetingSummaries
		- Summary[1..2] (rest is missing unfortunately)
* PrelimenaryProjectReport
	- BachelorForprojekt (the prelimnary project report completed before project initiation)
* ProcessReport
	- Process report (description of the projects process management etc.)
* ProjectPlanning
	- Iterations plan (The initial overall iteration plan)
	- Risikovurdering (Risk assesment plan)
	- Projektplan_V0.2 (Newest version of the project plan)
	- Samarbejds aftale (Cooperation agreement)
* Requirements
	- Kravspecifikation (Requirement specification)
* Specificiations
	- Protocol (specification of the AIOCA protocol used for communication between devices)
	- UserSettingsSpecification (specification of the supported user-settings)
* TestDocs
	- Accepttest
		- CompletedAccepttest
		- AccepttestSpecification
	- IntegrationTest
	- MainController_Unittest
	- Smartphone_Unittest
	- Test_SupportCtrl
	- Test_WiFiChip
* TestApps
	- MainCtrl_ImageRetriever
		- get_image (python script used for verifying image quality on MainController)
	- SupportCtrl test apps
		- TestSocket (python script used for verifying image quality and FPS)
	- WiFi chip test apps
		- TestReceiveVideoFile (C++ program for testing data throughput from mainctrl to desktop)
		- CompileArguments (Compile arguments for compiling *TestReceiveVideoFile*)
		- PacketSenderPortable_TestSoftware (Software used for testing individual TCP request to the WiFiChip. This software was not provided by the group.)
	