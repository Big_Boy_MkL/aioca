#ifndef _VIDEOSTREAMCTRL_H_
#define _VIDEOSTREAMCTRL_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "Camera/OV2640.hpp"
#include "UDP/Socket.hpp"
#include "TCP/Socket.hpp"
#include "TimeCheck/TimeCheck.hpp"
#include "IStreamCtrl.hpp"

namespace VideoStream
{
    class StreamCtrl : public IStreamCtrl
    {
    private:
        QueueHandle_t tcpQueue_;

        UDP::ISocket* udpSocket_;
        Camera::OV2640* camera_;
        Utility::TimeCheck* timeout_;

        TaskHandle_t *runStreamHandle_ = NULL;

        in_addr_t CheckMessage(QueueHandle_t tcpQueue_);

    public:
        StreamCtrl(QueueHandle_t queue, UDP::ISocket* udp, Camera::OV2640* cam, Utility::TimeCheck* time);
        ~StreamCtrl();
        void Start();
        void Stop();
        void Run();
    };
} // namespace VideoStream

#endif