#ifndef _TASKWRAPPERS_H_
#define _TASKWRAPPERS_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "StreamCtrl.hpp"

namespace VideoStream
{
    // Wrapper to use class method in task
    static void StreamCtrlRunWrapper(void *param)
    {
        static_cast<StreamCtrl *>(param)->Run();
    }
} // namespace VideoStream

#endif