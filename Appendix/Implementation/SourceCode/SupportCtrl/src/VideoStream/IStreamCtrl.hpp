#ifndef _IVIDEOSTREAMCTRL_H_
#define _IVIDEOSTREAMCTRL_H_

namespace VideoStream
{
    class IStreamCtrl
    {
    public:
        void Start();
        void Stop();
        void Run();
    };
} // namespace VideoStream

#endif