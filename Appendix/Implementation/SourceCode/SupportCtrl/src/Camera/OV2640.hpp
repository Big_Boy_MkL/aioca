#ifndef _OV2640_H_
#define _OV2640_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "esp_camera.h"

/**
 * Private Headers
 */
#include "ICamera.hpp"

namespace Camera
{
    class OV2640 : public ICamera
    {
    private:
        camera_fb_t *picturePtr;

    public:
        OV2640();
        ~OV2640();
        uint8_t *TakePicture(size_t *length);
        void ReturnPictureBuf();
    };
} // namespace OV2640

#endif