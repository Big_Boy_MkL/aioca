#ifndef _TCPSCOKET_H_
#define _TCPSCOKET_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "ISocket.hpp"

namespace TCP
{
    class Socket : public ISocket
    {
    public:
        Socket(QueueHandle_t queueHandle);
        ~Socket();

        /**
	* \brief Starts the socket task and starts the packethandler task. 
	*/
        void Start();

        /**
	* \brief Stops the socket task and stops the packethandler task. 
	*/
        void Stop();

        /**
	* \brief Task function that is used to continuously listen for
    *        new connections. If a connection is establied a task
    *        running \ref CheckMessageReceived will be started. 
	*/
        void listenSocket();

        /**
	* \brief Sends a message to a child socket. 
    *
    * \param[in] sock       Child socket id to send message to.
    * \param[in] message    Pointer to the message.
    * \param[in] len        Length of message to be sent.
    * \param[in] done       Boolean to determinde if child socket should be shutdown after message.
	*/
        void SendMessage(int *sock, uint8_t *message, int len, bool done);

        /**
	* \brief Sends a error to a child socket. 
    *
    * \param[in] sock       Child socket id to send message to.
    * \param[in] error      Error command which can be found in xxxx.
	*/
        void SendError(int *sock, uint8_t error);

    private:
        /**
     * Variable to hold socket id. 
     */
        int listen_sock = 0;

        /**
     * Struct to hold number of max seconds to wait for a message
     */
        struct timeval tv = {
            .tv_sec = 5,
            .tv_usec = 0,
        };

        /**
     * Handles for tasks. 
     */
        TaskHandle_t *listenTaskHandle = NULL;

        /**
     * Handle for queue. 
     */
        QueueHandle_t messageQueue_ = NULL;

        void ReceiveMessage(int *sock, in_addr_t addr);
        void Shutdown(int *sock);
    };
} // namespace TCP

#endif