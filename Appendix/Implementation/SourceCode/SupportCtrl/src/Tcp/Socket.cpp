
/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "lwip/sockets.h"

/**
 * Private Headers
 */
#include "Socket.hpp"
#include "TCPTaskWrappers.hpp"
#include "GlobalDefines.hpp"

namespace TCP
{

    Socket::Socket(QueueHandle_t queueHandle) : messageQueue_(queueHandle)
    {
        // Set up socket ip address and port
        struct sockaddr_in dest_addr_ip4;
        dest_addr_ip4.sin_addr.s_addr = htonl(INADDR_ANY);
        dest_addr_ip4.sin_family = AF_INET;
        dest_addr_ip4.sin_port = htons(TCPSOCKET_SOCKETPORT);

        // Create a TCP socket
        listen_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (listen_sock < 0)
        {
            // Return if failed
            return;
        }

        // Bind socket to ip address defined above
        if (bind(listen_sock, (struct sockaddr *)&dest_addr_ip4, sizeof(dest_addr_ip4)) != 0)
        {
            // Return if failed
            return;
        }

        // Make socket listen for incomming connections
        if (listen(listen_sock, TCPSOCKET_MAX_CONNECTIONS) != 0)
        {
            // Return if failed
            return;
        }
    }

    /**************************************************************************************************/

    Socket::~Socket()
    {
        // Close TCP socket and delete tasks
        close(listen_sock);

        if (listenTaskHandle != NULL)
        {
            vTaskDelete(listenTaskHandle);
        }
    }

    /**************************************************************************************************/

    void Socket::Start()
    {
        // Only start task if TCP socket has been created
        if (listen_sock != 0)
        {
            // Start listen and packet handler tasks
            xTaskCreate(TCPSocketListenWrapper, "TCP_Socket", 2048, this, 5, listenTaskHandle);
        }
    }

    /**************************************************************************************************/

    void Socket::Stop()
    {
        // Stop listen and packet handler tasks
        vTaskDelete(listenTaskHandle);
    }

    /**************************************************************************************************/

    void Socket::listenSocket()
    {
        while (1)
        {
            struct sockaddr source_addr;
            uint addr_len = sizeof(source_addr);

            // Accept incomming TCP connection
            int sock = accept(listen_sock, &source_addr, &addr_len);

            if (sock > 0)
            {
                in_addr_t ipAddr = ((struct sockaddr_in *)&source_addr)->sin_addr.s_addr;

                // Create a task to check if TCP connection is being used.
                ReceiveMessage(&sock, ipAddr);
            }
        }
    }

    /**************************************************************************************************/

    void Socket::SendError(int *sock, uint8_t error)
    {
        // Error message
        uint8_t data[3] = {PROTOCOL_START, error, PROTOCOL_END};

        // Send error and shutdown child.
        SendMessage(sock, data, 3, true);
    }

    /**************************************************************************************************/

    void Socket::SendMessage(int *sock, uint8_t *message, int len, bool done)
    {
        // Send message
        send(*sock, message, len, 0);

        // Shutdown socket if flag is set
        if (done)
        {
            Shutdown(sock);
        }
    }

    /***************************************************************************************************
 * Private methods
 ***************************************************************************************************/

    void Socket::ReceiveMessage(int *sock, in_addr_t addr)
    {
        while (1)
        {
            // Create a buffer for incomming msg
            uint8_t buffer[WIFI_RECEIVE_BUFFER_SIZE];
            
            // Receive message
            int len = recv(*sock, buffer, WIFI_RECEIVE_BUFFER_SIZE - 1, 0); //TODO check length
            if (len < 0)
            {
                Shutdown(sock);
                break;
            }
            else if (len > 0)
            {        
                // Create data stuckture to put into queue
                MessageQueueData data = {
                    .message = buffer,
                    .len = len,
                    .sock = *sock,
                    .socketPtr = this,
                    .addr = addr,
                };

                // Put msg in queue
                if (xQueueSend(messageQueue_, (void *)&data, (TickType_t)10) != pdPASS)
                {
                    // Send error
                    SendError(sock, PROTOCOL_ERROR_MSGQUEUEFULL);
                }
            }
        }
    }

    /**************************************************************************************************/

    void Socket::Shutdown(int *sock)
    {
        // Shutdown and close child socket
        shutdown(*sock, 0);
        close(*sock);
    }
} // namespace TCP