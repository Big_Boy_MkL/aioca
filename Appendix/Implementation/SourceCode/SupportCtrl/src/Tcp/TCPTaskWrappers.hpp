#ifndef _TCPTASKWRAPPERS_H_
#define _TCPTASKWRAPPERS_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "ISocket.hpp"

namespace TCP
{
    // Wrapper to use class method in task
    void TCPSocketListenWrapper(void *param)
    {
        static_cast<ISocket *>(param)->listenSocket();
    }
} // namespace TCP

#endif