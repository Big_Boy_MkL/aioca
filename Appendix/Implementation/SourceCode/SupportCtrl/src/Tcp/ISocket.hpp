#ifndef _ITCPSCOKET_H_
#define _ITCPSCOKET_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */

namespace TCP
{
    class ISocket
    {
    public:
        virtual void Start() = 0;
        virtual void Stop() = 0;
        virtual void listenSocket() = 0;
        virtual void SendMessage(int *sock, uint8_t *message, int len, bool done) = 0;
        virtual void SendError(int *sock, uint8_t error) = 0;
    };

    struct MessageQueueData
    {
        uint8_t *message;
        int len;
        int sock;
        ISocket *socketPtr;
        in_addr_t addr;
    };
} // namespace TCP

#endif