#ifndef _WIFICONTROLLER_H_
#define _WIFICONTROLLER_H_

/**
 * C++ Libs
 */
#include <iostream>
#include <cstring>

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "esp_wifi.h"

/**
 * Private Headers
 */

namespace WIFI
{
    class WifiController
    {
    public:
        WifiController();
        ~WifiController();

        /**
	* \brief Sets the wireless driver to AP mode (Access point). 
    *
    * \param[in] ssidToCreate   Desired name for the AP.
    * \param[in] pass           Desired password for the AP.
	*/
        void SetApMode(std::string ssidToCreate, std::string pass);

        /**
	* \brief Sets the wireless driver to AP mode (Access point). 
    *
    * \param[in] ssidToJoin     Name of AP to join.
    * \param[in] pass           Password for AP to join.
	*/
        void SetStaMode(std::string ssidToJoin, std::string pass, uint8_t ip);

        /**
	* \brief Starts the wifi module
	*/
        void Start();

        /**
	* \brief Stops the wifi module
	*/
        void Stop();
    };
} // namespace WIFI

#endif