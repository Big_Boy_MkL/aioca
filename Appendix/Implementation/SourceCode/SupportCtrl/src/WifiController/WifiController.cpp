/**
 * C++ Libs
 */
#include <iostream>
#include <cstring>

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

/**
 * ESP Libs
 */
#include "esp_event.h"

/**
 * Private Headers
 */
#include "WifiController.hpp"

namespace WIFI
{
    static void event_handler(void *arg, esp_event_base_t event_base,
                              int32_t event_id, void *event_data)
    {
        if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
        {
            // Network found - Connect to network
            esp_wifi_connect();
        }
        else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED)
        {
            // Connected
        }
        else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
        {
            // Try to connect again
            esp_wifi_connect();
        }
    }

    WifiController::WifiController()
    {
        // Create a default setup for WIFI driver
        wifi_init_config_t initConfig = WIFI_INIT_CONFIG_DEFAULT();
        initConfig.dynamic_tx_buf_num = 256;
        initConfig.static_tx_buf_num = 64;
        initConfig.tx_ba_win = 32;
        initConfig.dynamic_rx_buf_num = 8;
        initConfig.static_rx_buf_num = 8;
        initConfig.rx_ba_win = 8;

        // Initialize wifi driver
        ESP_ERROR_CHECK(esp_wifi_init(&initConfig));
    }

    WifiController::~WifiController()
    {
        // Deinitilize driver
        esp_wifi_deinit();
    }

    /**************************************************************************************************/

    void WifiController::SetApMode(std::string ssidToCreate, std::string pass)
    {
        // Setup DHCP
        ESP_ERROR_CHECK(esp_netif_init());
        ESP_ERROR_CHECK(esp_event_loop_create_default());
        esp_netif_create_default_wifi_ap();

        // Set Accesspoint mode
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
        wifi_config_t wifiConfig = wifi_config_t();

        // Setup wireless settings - SSID, Password, Channel.
        MEMCPY(wifiConfig.ap.ssid, ssidToCreate.c_str(), ssidToCreate.length() + 1);
        MEMCPY(wifiConfig.ap.password, pass.c_str(), pass.length() + 1);
        wifiConfig.ap.channel = 2;
        wifiConfig.ap.ssid_hidden = 0;
        wifiConfig.ap.authmode = WIFI_AUTH_WPA2_PSK;
        wifiConfig.ap.max_connection = 8;

        // Apply configuration
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifiConfig));
    }

    /**************************************************************************************************/

    void WifiController::SetStaMode(std::string ssidToJoin, std::string pass, uint8_t ip)
    {
        // Client
        ESP_ERROR_CHECK(esp_netif_init());
        ESP_ERROR_CHECK(esp_event_loop_create_default());
        esp_netif_t *testptr = esp_netif_create_default_wifi_sta();
        esp_netif_dhcpc_stop(testptr);

        esp_netif_ip_info_t ip_info;

        IP4_ADDR(&ip_info.ip, 192, 168, 4, ip);
        IP4_ADDR(&ip_info.gw, 192, 168, 4, 1);
        IP4_ADDR(&ip_info.netmask, 255, 255, 255, 0);

        esp_netif_set_ip_info(testptr, &ip_info);

        // Set STA mode
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

        // Add event so its possible to connect and its possible to reconnect if network disconnects.
        ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));

        wifi_config_t wifiConfig = wifi_config_t();
        // Setup wireless settings - SSID, Password.
        MEMCPY(wifiConfig.sta.ssid, ssidToJoin.c_str(), ssidToJoin.length() + 1);
        MEMCPY(wifiConfig.sta.password, pass.c_str(), pass.length() + 1);
        wifiConfig.sta.pmf_cfg.capable = true;
        wifiConfig.sta.pmf_cfg.required = false;
        wifiConfig.sta.listen_interval = 20;

        // Apply configuration
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig));
    }

    /**************************************************************************************************/

    void WifiController::Start()
    {
        // Start wifi module
        ESP_ERROR_CHECK(esp_wifi_start());
    }

    /**************************************************************************************************/

    void WifiController::Stop()
    {
        // Stop wifi module
        ESP_ERROR_CHECK(esp_wifi_stop());
    }
} // namespace WIFI