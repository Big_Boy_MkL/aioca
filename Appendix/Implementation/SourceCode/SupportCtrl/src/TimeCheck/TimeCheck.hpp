#ifndef _TIMECHECK_H_
#define _TIMECHECK_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

#include "driver/timer.h"

/**
 * Private Headers
 */

namespace Utility
{
    class TimeCheck
    {
    private:
        timer_group_t group_;
        timer_idx_t index_;
        double timeOut_;
    public:
        TimeCheck(timer_group_t group, timer_idx_t index, double timeout);
        ~TimeCheck();
        void Start();
        bool CheckTimeout();
        void FeedTimeCheck();
    };
} // namespace TimeOut

#endif