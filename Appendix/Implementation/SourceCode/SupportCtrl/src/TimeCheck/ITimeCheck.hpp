#ifndef _ITIMECHECK_H_
#define _ITIMECHECK_H_

namespace Utility
{
    class TimeCheck
    {
    public:
        virtual void Start() = 0;
        virtual bool CheckTimeout() = 0;
        virtual void FeedTimeCheck() = 0;
    };
} // namespace TimeOut

#endif