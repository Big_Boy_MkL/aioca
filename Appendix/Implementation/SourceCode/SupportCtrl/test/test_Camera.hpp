#ifndef _TEST_CAMERA_H_
#define _TEST_CAMERA_H_

#include <unity.h>
#include "Camera/OV2640.hpp"

void test_CameraGetBuffer()
{
    auto cam = Camera::OV2640();

    uint8_t* buf = NULL;
    size_t len = 0;

    buf = cam.TakePicture(&len);
    TEST_ASSERT_NOT_EQUAL(NULL, buf);
}

void test_CameraGetLength()
{
    auto cam = Camera::OV2640();

    size_t len = 0;
    uint8_t* buf = cam.TakePicture(&len);
    TEST_ASSERT_NOT_EQUAL(0, len);
}

void test_OV2640()
{
    RUN_TEST(test_CameraGetBuffer);
    RUN_TEST(test_CameraGetLength);
}

#endif