#include <unity.h>
#include "test_Camera.hpp"
#include "test_TimeCheck.hpp"

void setUp(void)
{
}

void tearDown(void)
{
}

extern "C" 
{
    void app_main();
}

void app_main()
{ 
    UNITY_BEGIN();

    test_OV2640();
    test_TimeCheck();

    UNITY_END(); // stop unit testing 

    while (1)
    {
    }
}