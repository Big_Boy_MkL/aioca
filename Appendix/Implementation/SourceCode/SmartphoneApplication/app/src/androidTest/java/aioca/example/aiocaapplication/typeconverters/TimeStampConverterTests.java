package aioca.example.aiocaapplication.typeconverters;

import org.junit.Test;

import aioca.example.aiocaapplication.videoinfo.models.TimeStamp;
import aioca.example.aiocaapplication.videoinfo.models.TimeStampConverters;
import static org.junit.Assert.assertEquals;

public class TimeStampConverterTests {

    String pattern = "MM:dd:yyyy:HH:mm";

    String testDay = "03:23:2020:23:32";
    TimeStamp testTimeStamp = new TimeStamp(2020,3,23,23,32);


    @Test
    public void testStringToTimeStamp(){
        TimeStamp timeStamp = TimeStampConverters.fromStringToTimeStamp(testDay);

        assertEquals(testTimeStamp.getYear(), timeStamp.getYear());
        assertEquals(testTimeStamp.getDay(), timeStamp.getDay());
        assertEquals(testTimeStamp.getMonth(), timeStamp.getMonth());
        assertEquals(testTimeStamp.getHour(), timeStamp.getHour());
        assertEquals(testTimeStamp.getMinute(), timeStamp.getMinute());
    }


    @Test
    public void testTimeStampToString(){
        String timeStampString = TimeStampConverters.fromTimeStampToString(testTimeStamp);
        assertEquals(testDay, timeStampString);
    }

}
