package aioca.example.aiocaapplication.intregration_tests;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.concurrent.CountDownLatch;

import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.settings.AiocaSettings;
import aioca.example.aiocaapplication.settings.AiocaSettings.UserSettings;
import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.util.ServiceHandle;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import static org.junit.Assert.*;

public class IntegrationTest_MainCtrl {

    Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    ServiceHandle serviceHandle = ServiceHandle.getInstance(context);

    private void startServiceTest(){
        Intent serviceIntent = new Intent(context, AiocaNetworkService.class);
        context.startService(serviceIntent);
    }

    private void stopServiceTest(){
        Intent AiocaServiceIntent = new Intent(context, AiocaNetworkService.class);
        context.stopService(AiocaServiceIntent);
    }

    @Test
    public void test_requestVideoInfo(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();

        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                aiocaNetworkService.requestVideoInfo(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }
                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[18] = 4;
                        assertArrayEquals(datagramPacket.getData(), testPackage);
                        onServiceConnectedSignal.countDown();
                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                }, 69);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_requestNumOfVid(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();

        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                aiocaNetworkService.requestNumOfVid(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }
                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[2] = 4;
                        assertEquals(datagramPacket.getData()[0], testPackage[0]);
                        assertEquals(datagramPacket.getData()[2], testPackage[2]);
                        onServiceConnectedSignal.countDown();
                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_sendUserID(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                try {
                    aiocaNetworkService.sendUserID(new SingleObserver<DatagramPacket>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                            byte[] testPackage = new byte[64];
                            testPackage[0] = 1;
                            testPackage[1] = 6;
                            testPackage[2] = 4;
                            assertArrayEquals(datagramPacket.getData(), testPackage);
                            onServiceConnectedSignal.countDown();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }
                    }, "2LQPKB0ESzTIWfN44rsW29L4HLK2");
                } catch (IOException e) {
                    e.printStackTrace();
                    fail();
                    onServiceConnectedSignal.countDown();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_sendUserID_Fail(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                try {
                    aiocaNetworkService.sendUserID(new SingleObserver<DatagramPacket>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                            byte[] testPackage = new byte[64];
                            testPackage[0] = 1;
                            testPackage[1] = 21;
                            testPackage[2] = 4;
                            assertArrayEquals(datagramPacket.getData(), testPackage);
                            onServiceConnectedSignal.countDown();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }
                    }, "2LQPKB0ESzTIWfN44rsW29L4HLK\0");
                } catch (IOException e) {
                    e.printStackTrace();
                    fail();
                    onServiceConnectedSignal.countDown();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_sendUserSetting(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                aiocaNetworkService.sendUserSetting(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[1] = 1;
                        testPackage[2] = 4;
                        assertArrayEquals(datagramPacket.getData(), testPackage);
                        onServiceConnectedSignal.countDown();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                    }, new SettingsClass(UserSettings.parkingCamera.ordinal(),
                            AiocaSettings.aiocaSettings.get(UserSettings.parkingCamera),
                        true, -1));
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_receiveUserSettings(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                aiocaNetworkService.receiveUserSettings(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[2] = 4;
                        assertEquals(datagramPacket.getData()[0], testPackage[0]);
                        assertEquals(datagramPacket.getData()[2], testPackage[2]);
                        onServiceConnectedSignal.countDown();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                }, UserSettings.parkingCamera);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }
}
