package aioca.example.aiocaapplication.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.networking.observables.TcpSingle;
import aioca.example.aiocaapplication.networking.observables.UdpObservables;
import aioca.example.aiocaapplication.networking.observers.VideoObserver;
import aioca.example.aiocaapplication.networking.controllers.TcpController;
import aioca.example.aiocaapplication.networking.controllers.UdpController;
import aioca.example.aiocaapplication.networking.controllers.WiFiController;
import aioca.example.aiocaapplication.settings.AiocaSettings;
import aioca.example.aiocaapplication.settings.SettingsClass;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

import static aioca.example.aiocaapplication.networking.util.IpTools.getSmartPhonesIp;

public class AiocaNetworkService extends Service {
    /**
     * Alot of the General Service Implementation is taken from SMAP Assignment 2, 2019
     * Developed by Mikkel Jensen
     * RxJava
     */
    private final static String     TAG = "AiocaNetworkService";

    private final static String     DEFAULT_NETWORK_NAME    = "AIOCA";
    private final static String     DEFAULT_NETWORK_PASS    = "12345678";

    private final static String     START_VIDEO_STRING      = "01ff04";
    private final static String     START_KEEP_ALIVE_STRING = "01fe04";

    private final static int        UDP_PORT                = 2000;
    private final static int        TCP_PORT                = 2000;
    private final static String     MAINCONTROLLER_IP       = "192.168.4.1";
    private final static String     CAMERA_IP               = "192.168.4.254";
    private final static int        UDP_SO_TIMEOUT          = 2000;

    protected final static int      NOTIFICATION_AIOCA_ID   = 144;

    private InetAddress             CAMERA_IP_INET_ADDRESS;
    private InetAddress             MAINCONTROLLER_IP_INET_ADDRESS;



    private boolean                 isStarted = false;

    public UdpController            udpController;

    public WiFiController           wiFiController;
    private WifiManager             wifiManager;

    public TcpSingle                tcpObservables;
    public UdpObservables           udpObservables;

    VideoObserver                   videoObserver;


    public class AiocaNetworkServiceBinder extends Binder{
        public AiocaNetworkService getService(){
            return AiocaNetworkService.this;
        }
    }

    private final IBinder mBinder = new AiocaNetworkServiceBinder();

    public AiocaNetworkService() throws UnknownHostException {
        CAMERA_IP_INET_ADDRESS = InetAddress.getByName(CAMERA_IP);
        MAINCONTROLLER_IP_INET_ADDRESS = InetAddress.getByName(MAINCONTROLLER_IP);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        tcpObservables = new TcpSingle(this);
        udpObservables = new UdpObservables(this);

        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        /**
         * Convert String IP of Camera to Inet Address Representation
         */
        try {
            CAMERA_IP_INET_ADDRESS = InetAddress.getByName(CAMERA_IP);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        wiFiController = new WiFiController(DEFAULT_NETWORK_NAME,
                DEFAULT_NETWORK_PASS,this, connMgr, wifiManager);

    }

    /**
     * Returns Binder Object when Fragment or Activity Binds to Service
     */
    @Nullable
    @Override
    public IBinder onBind(@NonNull Intent intent) {
        return mBinder;
    }

    /**
     * What will run on Service Start
     * Will Create an Notification in the case and start the Service as a START_STICKY
     * Would prob be looked into if this needs changing
     */
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        if (!isStarted && intent != null){
            isStarted = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel myChannel = new NotificationChannel(
                        getString(R.string.service_notification_key),
                        getString(R.string.service_notification_name),
                        NotificationManager.IMPORTANCE_LOW);

                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(myChannel);
            }

            Notification notification = new NotificationCompat.Builder(this,
                    getString(R.string.service_notification_key))

                    .setContentTitle("Aioca is running")
                    .setContentText(getString(R.string.service_notification_description))
                    .setSmallIcon(R.drawable.ic_aioca_no_name_background)
                    .setTicker("Aioca")
                    .setChannelId(getString(R.string.service_notification_key))
                    .build();
            startForeground(NOTIFICATION_AIOCA_ID, notification);
        }
        return START_STICKY;
    }

    /**
     * onDestroy
     * sets isStarted to False
     */
    @Override
    public void onDestroy() {
        isStarted = false;
        super.onDestroy();
    }

    /**
     * Will request a video from the MainController using a TCP request.
     * @param requestVideoObserver A observer that will be called after the network request
     *                             is sent.
     * @param index                The index of the video that should be downloaded.
     */
    public void requestVideo(SingleObserver<DatagramPacket> requestVideoObserver, int index){
        Log.d(TAG, "requestNumOfVid: Called");

        StringBuilder builder = new StringBuilder();
        builder.append("01ff14");
        if (index < 10)
            builder.append("0");
        builder.append(index);
        builder.append("3b04");

        tcpObservables.sendTcpRequest(builder.toString(), new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(requestVideoObserver);
    }

    /**
     * Will request information about a specific video from the MainController using a TCP request.
     * @param videoInfoObserver A observer that will be called after the network request
     *                          is sent.
     */
    public void requestVideoInfo(SingleObserver<DatagramPacket> videoInfoObserver, int index){
        Log.d(TAG, "requestNumOfVid: Called");

        StringBuilder builder = new StringBuilder();
        builder.append("01ff12");
        if (index < 10)
            builder.append("0");
        builder.append(index);
        builder.append("04");

        tcpObservables.sendTcpRequest(builder.toString(), new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(videoInfoObserver);
    }

    /**
     * Will send a start ACK TCP request to MainController. This should be used when receiving a
     * video from the MainController.
     * @param videoInfoObserver A observer that will be called after the network request
     *                          is sent.
     */
    public void requestVideoInfo_ACK(SingleObserver<DatagramPacket> videoInfoObserver){
        Log.d(TAG, "requestNumOfVid: Called");
        tcpObservables.sendTcpRequest("010604", new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(videoInfoObserver);
    }

    /**
     * Will send a start NAK TCP request to MainController. This should be used when receiving a
     * video from the MainController.
     * @param videoInfoObserver A observer that will be called after the network request
     *                          is sent.
     */
    public void requestVideoInfo_NAK(SingleObserver<DatagramPacket> videoInfoObserver){
        Log.d(TAG, "requestNumOfVid: Called");
        tcpObservables.sendTcpRequest("011504", new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(videoInfoObserver);
    }

    /**
     * Will request the number of stored videos in the MainController using a TCP request.
     * @param numOfSavedVidObserver A observer that will be called after the network request
     *                              is sent.
     */
    public void requestNumOfVid(SingleObserver<DatagramPacket> numOfSavedVidObserver){
        Log.d(TAG, "requestNumOfVid: Called");

        tcpObservables.sendTcpRequest("01ff1104", new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(numOfSavedVidObserver);
    }

    /**
     * Will request a user setting from the MainController using a TCP request.
     * @param settingsData A observer that will be called after the network request
     *                     is sent.
     * @param  settingId   The ID of the setting that should be requested.
     */
    public void receiveUserSettings(SingleObserver<DatagramPacket> settingsData, AiocaSettings.UserSettings settingId){
        String bufmes = "";
        if (settingId.ordinal() < 10){
            bufmes = bufmes + "0";
        }
        bufmes = bufmes + settingId.ordinal();

        tcpObservables.sendTcpRequest("01ff10" + bufmes + "04", new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(settingsData);
    }

    /**
     * Will send a user setting to the MainController using a TCP request.
     * @param settingsObserver A observer that will be called after the network request
     *                           is sent.
     * @param settingsClass   Class containing setting information that should be sent.
     */
    public void sendUserSetting(SingleObserver<DatagramPacket> settingsObserver, SettingsClass settingsClass){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("01");
        stringBuilder.append("00");
        stringBuilder.append("10");

        if (settingsClass.getSettingId() < 10){
            stringBuilder.append("0");
        }
        stringBuilder.append(settingsClass.getSettingId());

        if (settingsClass.isEnabled()){
            stringBuilder.append("01");
        }else {
            stringBuilder.append("00");
        }
        stringBuilder.append("04");

        tcpObservables.sendTcpRequest(stringBuilder.toString(), new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(settingsObserver);

    }

    /**
     * Will send the user ID to the MainController using a TCP request.
     * @param sendUserIDObserver A observer that will be called after the network request
     *                           is sent.
     */
    public void sendUserID(SingleObserver<DatagramPacket> sendUserIDObserver, String ID) throws IOException {
        Log.d(TAG, "sendUserID: Called");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(0x01);
        baos.write(0x00);
        baos.write(0x45);
        baos.write(ID.getBytes(StandardCharsets.UTF_8));
        baos.write(0x04);

        Log.d(TAG, "sendUserID: Called         " + Arrays.toString(baos.toByteArray()));

        tcpObservables.sendTcpRequest(baos.toByteArray(), new TcpController(), MAINCONTROLLER_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sendUserIDObserver);
    }



    /**
     * Will send a start TCP request to Camera and use startVideoObservers as Observer.
     * @param startVideoObserver Observer<DatagramPacket></DatagramPacket>.
     */
    public void startVideoFeed(SingleObserver<DatagramPacket> startVideoObserver){
        Log.d(TAG, "startVideoFeed: Called");
        tcpObservables.sendTcpRequest(START_VIDEO_STRING, new TcpController(), CAMERA_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(startVideoObserver);
    }

    /**
     * Will Send a TcpSingle Request to the Camera to Keep the Sockets Alive
     * Using simple Single Observer that doesn't need to react onSuccess or onError
     * Maybe onError Should do Something..? ;)
     */
    public void startSendKeepAlive(SingleObserver<DatagramPacket> singleObserver){
        tcpObservables.sendTcpRequest(START_KEEP_ALIVE_STRING, new TcpController(), CAMERA_IP_INET_ADDRESS, TCP_PORT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(singleObserver);
    }

    /**
     * Will Start the UDP Receive Video Feed with AiocaNetworkServices VideoObserver
     */
    public void startUDPFeed() throws IOException{
        /**
         * Instantiating the UdpController
         */
        if (udpController == null)
        {

            udpController = UdpController.getInstance();
            udpController.Setup(wiFiController.getConnectedNetwork(), getSmartPhonesIp(wifiManager) , UDP_PORT);

        }else {
            if (!udpController.IsAlive()){

                udpController.Setup(wiFiController.getConnectedNetwork(), getSmartPhonesIp(wifiManager) , UDP_PORT);

            }
        }

        if (videoObserver != null){
            udpObservables.getVideoFeed()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(videoObserver);
        }
    }

    /**
     * Will set the AiocaNetworkServices VideoObserver
     * @param videoObserver VideoObserver that will Observer on UDP Receive Feed
     */
    public void setVideoObserver(VideoObserver videoObserver) {
        this.videoObserver = videoObserver;
    }

    /**
     * Read Data using the Controllers of the Service
     * @param datagramPacket Datagram Packer to put data into
     * @return Returns the length of read data as int
     * @throws IOException If read operation fails
     */
    public int readUdp(DatagramPacket datagramPacket) throws IOException{
        if (udpController.getSoTimeout() != UDP_SO_TIMEOUT){
            udpController.setSoTimeout(UDP_SO_TIMEOUT);
        }

        udpController.Read(datagramPacket);
        return datagramPacket.getLength();
    }
}
