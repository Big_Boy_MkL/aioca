package aioca.example.aiocaapplication.networking.util;

import android.net.wifi.WifiManager;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpTools {
    /**
     * Inspiration from
     * https://stackoverflow.com/questions/20846120/alternative-for-formatter-formatipaddressint
     * Reverse byte Array
     * https://stackoverflow.com/questions/2137755/how-do-i-reverse-an-int-array-in-java
     * @param wifiManager wifiManager Object
     * @return Ip Address of the Phone
     */
    public static InetAddress getSmartPhonesIp(WifiManager wifiManager) throws UnknownHostException{
        byte[] ipAddress = BigInteger.valueOf(wifiManager.getConnectionInfo().getIpAddress()).toByteArray();
        for(int i = 0; i < ipAddress.length / 2; i++)
        {
            byte temp = ipAddress[i];
            ipAddress[i] = ipAddress[ipAddress.length - i - 1];
            ipAddress[ipAddress.length - i - 1] = temp;
        }
        return InetAddress.getByAddress(ipAddress);
    }
}
