package aioca.example.aiocaapplication.networking.observers.settingsobservers;

import android.util.Log;

import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentLinkedQueue;

import aioca.example.aiocaapplication.settings.AiocaSettings;
import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.viewmodels.SettingsViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class RoadStripAlertSettingsObserver implements SingleObserver<DatagramPacket> {

    SettingsViewModel settingsViewModel;
    ConcurrentLinkedQueue<AiocaSettings.UserSettings> concurrentLinkedQueue;
    private String TAG = "RoadStripAlertSettingsObserver";

    public RoadStripAlertSettingsObserver(SettingsViewModel settingsViewModel, ConcurrentLinkedQueue<AiocaSettings.UserSettings> concurrentLinkedQueue) {
        this.settingsViewModel = settingsViewModel;
        this.concurrentLinkedQueue = concurrentLinkedQueue;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Called when the TCP request have been sent successfully - Inserts setting into database.
     * @param datagramPacket A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {

        byte[] buf = datagramPacket.getData();
        boolean bufBool = false;

        if (buf[1] == 1){
            bufBool = true;
        }
        int settingsId_ = AiocaSettings.UserSettings.roadStripAlert.ordinal();
        // Insert can be used as it have different onClash Handling now!
        settingsViewModel.insertSetting(new SettingsClass(settingsId_,
                AiocaSettings.aiocaSettings.get(settingsId_),bufBool, -1));
        Log.d(TAG, "onSuccess: Received Road Strip Alert Setting");
        concurrentLinkedQueue.remove(AiocaSettings.UserSettings.roadStripAlert);
    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */

    @Override
    public void onError(@NonNull Throwable e) {

    }
}
