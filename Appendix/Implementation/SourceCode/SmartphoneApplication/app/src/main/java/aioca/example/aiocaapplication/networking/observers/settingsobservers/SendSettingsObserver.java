package aioca.example.aiocaapplication.networking.observers.settingsobservers;

import android.util.Log;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.net.DatagramPacket;
import java.util.Arrays;

import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.viewmodels.SettingsViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class SendSettingsObserver implements SingleObserver<DatagramPacket> {

    private static String TAG = "SendSettingsObserver";

    SettingsViewModel settingsViewModel;
    SettingsClass settingsClass;
    SwitchMaterial switchMaterial;

    /**
     * Constructor for SendSettingsObserver
     * @param settingsViewModel SettingsViewModel for inserting to database
     * @param settingsClass SettingClass to send
     * @param switchMaterial Switch Material to reset if error happens
     */
    public SendSettingsObserver(SettingsViewModel settingsViewModel, SettingsClass settingsClass, SwitchMaterial switchMaterial) {
        this.settingsViewModel = settingsViewModel;
        this.settingsClass = settingsClass;
        this.switchMaterial = switchMaterial;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Insert Setting into Database
     * @param datagramPacket Receive data
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
        byte[] buf = datagramPacket.getData();

        Log.d(TAG, "onSuccess: buf[0]: " + buf[0]);
        if (buf[0] == 1){
            // Updated from update to Insert
            settingsViewModel.insertSetting(settingsClass);
        }
        else {
            switchMaterial.setChecked(!switchMaterial.isChecked());
            Log.e(TAG, "onSuccess: couldn't change setting on MainCtrl"+ Arrays.toString(buf));
        }
    }

    /**
     * Reset Switch on error
     * @param e Throwable e
     */
    @Override
    public void onError(@NonNull Throwable e) {
        switchMaterial.setChecked(!switchMaterial.isChecked());
    }
}
