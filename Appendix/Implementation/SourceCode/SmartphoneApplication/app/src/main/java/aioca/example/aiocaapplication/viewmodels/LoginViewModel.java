package aioca.example.aiocaapplication.viewmodels;

import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * NOT USED
 * ViewModel for Login
 */
public class LoginViewModel extends ViewModel {

    private MutableLiveData<String> Email = new MutableLiveData<>();
    private MutableLiveData<String> Password = new MutableLiveData<>();

    public LoginViewModel() {
    }


    public MutableLiveData<String> getEmail() {
        return Email;
    }

    public void setEmail(MutableLiveData<String> email) {
        Email = email;
    }

    public MutableLiveData<String> getPassword() {
        return Password;
    }

    public void setPassword(MutableLiveData<String> password) {
        Password = password;
    }


}
