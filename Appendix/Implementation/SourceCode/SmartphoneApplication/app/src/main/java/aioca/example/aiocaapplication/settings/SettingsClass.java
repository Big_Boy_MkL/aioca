package aioca.example.aiocaapplication.settings;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 *  Model Class for Settings in the Application
 */
@Entity
public class SettingsClass {

    @PrimaryKey
    private int settingId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "isEnabled")
    private boolean isEnabled = false;
    @ColumnInfo(name = "settingsInt")
    private int settingsInt = -1;


    public SettingsClass(int settingId, String name, boolean isEnabled, int settingsInt){
        this.settingId = settingId;
        this.name = name;
        this.isEnabled = isEnabled;
        this.settingsInt = settingsInt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name_) {
        this.name = name_;
    }

    public void setSettingId(int settingId) {
        this.settingId = settingId;
    }

    public int getSettingId() {
        return settingId;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public void setSettingsInt(int settingsInt) {
        this.settingsInt = settingsInt;
    }

    public int getSettingsInt() {
        return settingsInt;
    }
}
