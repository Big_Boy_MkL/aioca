package aioca.example.aiocaapplication.videoencoder.executor;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.videoencoder.ImageToVideoConverter;
import aioca.example.aiocaapplication.videoencoder.InternalDataStorage;

public class EncodeVideoExecutor {
    ImageToVideoConverter converter;
    InternalDataStorage storage;


    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2, 0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

    public EncodeVideoExecutor(Context context){
        this.storage = new InternalDataStorage(context);
        this.converter = new ImageToVideoConverter(this.storage);
    }

    /**
     * Starts a thread that will convert the stored frames to a mp4 video
     * @param videoNumber The number of the video that should be converted.
     * @param numOfFrames Number of frames stored.
     */
    public void ConvertVideo(int videoNumber, int numOfFrames){
        threadPoolExecutor.execute(getInsertRunnable(videoNumber, numOfFrames));
    }

    private Runnable getInsertRunnable(int videoNumber, int numOfFrames){
        return () -> {
            try {
                converter.ConvertFramesIntoVideo(videoNumber, numOfFrames);
                storage.DeleteStoredVideoInfo(videoNumber);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }
}

