package aioca.example.aiocaapplication.settings.executors;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.settings.settingsdb.DaoSettingsInterface;

/**
 * NOT USED!
 */
public class GetAllSettingsExecutor {

    DaoSettingsInterface daoSettingsInterface;


    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2,0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>());

    public GetAllSettingsExecutor(DaoSettingsInterface daoSettingsInterface) {
        this.daoSettingsInterface = daoSettingsInterface;
    }

    public void getAllListSettings()
    {
        threadPoolExecutor.execute(getAllSettingsRunnable());
    }



    private Runnable getAllSettingsRunnable(){
        return () -> daoSettingsInterface.getAllList();
    }


}
