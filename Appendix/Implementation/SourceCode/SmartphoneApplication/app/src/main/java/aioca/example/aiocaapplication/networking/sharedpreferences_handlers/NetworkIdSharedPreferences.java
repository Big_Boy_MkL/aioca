package aioca.example.aiocaapplication.networking.sharedpreferences_handlers;

import android.content.Context;
import android.content.SharedPreferences;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.util.ServiceHandle;

public class NetworkIdSharedPreferences {

    private static NetworkIdSharedPreferences INSTANCE = null;
    private Context context;

    private NetworkIdSharedPreferences(Context context) {
        this.context = context;
    }

    public static NetworkIdSharedPreferences getInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = new NetworkIdSharedPreferences(context);
        }
        return INSTANCE;
    }

    /**
     * Clear the Network Id Shared Preference
     */
    public void clearNetworkIdSharedPreferences(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                String.valueOf(R.string.SharedPrefString),
                Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(String.valueOf(R.string.NetWorkIDString)).clear().apply();
    }

    /**
     * Will return the Network Id
     * @return Int, Network Id
     */
    public int getNetworkIdSharedPreferences(){
        return context.getSharedPreferences(String.valueOf(R.string.SharedPrefString),
                Context.MODE_PRIVATE)
                .getInt(String.valueOf(R.string.NetWorkIDString),-1);
    }

    /**
     * Set the Network Id shared Preference
     * @param netId Int, Network Id
     */
    public void setNetworkIdSharedPreferences(int netId){
        context.getSharedPreferences(String.valueOf(R.string.SharedPrefString),
                Context.MODE_PRIVATE)
                .edit()
                .putInt(String.valueOf(R.string.NetWorkIDString), netId)
                .apply();
    }

}
