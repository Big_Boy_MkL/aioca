package aioca.example.aiocaapplication.settings.settingsdb;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.settings.executors.InsertSettingExecutor;

/**
 * Inspiration taken from
 * https://developer.android.com/codelabs/android-training-livedata-viewmodel#14
 */
public class SettingsRepository {

    private DaoSettingsInterface daoSettingsInterface;
    LiveData<List<SettingsClass>> settingsLiveData;
    InsertSettingExecutor insertSettingExecutor;


    /**
     * Constructor for Repository
     * @param application Application
     */
    public SettingsRepository(Application application) {
        SettingsDatabase db = SettingsDatabase.getInstance(application);
        daoSettingsInterface = db.daoSettingsInterface();
        settingsLiveData = daoSettingsInterface.getAllLive();
        insertSettingExecutor = new InsertSettingExecutor(daoSettingsInterface);

    }

    /**
     * Get all settings
     * @return Livedata Object
     */
    public LiveData<List<SettingsClass>> getAllSettings(){
        return settingsLiveData;
    }

    /**
     * Insert into database
     * @param settingsClass SettingsClass
     */
    public void insert(SettingsClass settingsClass){
        insertSettingExecutor.insertSetting(settingsClass);
    }


}
