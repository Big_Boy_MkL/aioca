package aioca.example.aiocaapplication.networking.callback;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * https://issuetracker.google.com/issues/158718275
 * For more see WiFiController
 */

public class WiFiNetworkCallback extends ConnectivityManager.NetworkCallback {

    private final static String             TAG             = "WiFiNetworkCallback";
    private WifiManager                     wifiManager;

    private boolean                         isConnected     = false;
    private Network                         network;


    public WiFiNetworkCallback(WifiManager wifiManager) {
        this.wifiManager = wifiManager;

    }

    /**
     *  When a connection to the WiFi hotspot is successful
     * @param network Network
     */
    @Override
    public void onAvailable(Network network) {
        super.onAvailable(network);
        Log.d(TAG, "onAvailable: On Available called");
        isConnected = true;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
            this.network = network;
        }
        Log.d(TAG, "onAvailable: Bind Process to Network");
    }

    /**
     * When a connection to the Network is lost
     * @param network the network that is lost connection to
     */
    @Override
    public void onLost(Network network) {
        super.onLost(network);
        onNetworkDisconnect();
        Log.d(TAG, "onLost: Network Lost");
    }

    /**
     * When the Network is Unavailable
     */
    @Override
    public void onUnavailable() {
        super.onUnavailable();
        onNetworkDisconnect();
        Log.d(TAG, "onUnavailable: Network Unavailable");
    }

    /**
     * @return Network that is connected
     */
    public Network getNetwork() {
        return network;
    }

    /**
     * @return Boolean true if connected, false if not.
     */
    public boolean isConnected() {
        return isConnected;
    }


    private void onNetworkDisconnect(){
        /**
         * Will reset connectedNetwork and isConnected on network disconnect
         */
        isConnected = false;
        network = null;
    }

}
