package aioca.example.aiocaapplication.networking.controllers;

import android.net.Network;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public interface ISocketController {
    void Setup(Network network, InetAddress address, int port) throws IOException;
    void Destroy() throws IOException;
    int Read(DatagramPacket datagramPacket) throws IOException;
    void Write(DatagramPacket datagramPacket) throws IOException;
    boolean IsAlive();
}
