package aioca.example.aiocaapplication.settings.executors;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.settings.settingsdb.DaoSettingsInterface;


/**
 * https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ThreadPoolExecutor.html
 */
public class InsertSettingExecutor {

    DaoSettingsInterface daoSettingsInterface;

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2,0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

    /**
     * Constructor for Executor Wrapper
     * @param daoSettingsInterface Dao to use
     */
    public InsertSettingExecutor(DaoSettingsInterface daoSettingsInterface) {

        this.daoSettingsInterface = daoSettingsInterface;

    }

    /**
     * Insert Parameter into Database
     * @param settingsClass SettingsClass
     */
    public void insertSetting(SettingsClass settingsClass)
    {
        threadPoolExecutor.execute(getInsertRunnable(settingsClass));
    }

    /**
     * Runnable to run in executor
     * @param settingsClass SettingsClass
     * @return Runnable
     */
    private Runnable getInsertRunnable(final SettingsClass settingsClass){
        return () -> daoSettingsInterface.insertSetting(settingsClass);
    }

}
