package aioca.example.aiocaapplication.settings;

import java.util.HashMap;

public class AiocaSettings {

    public enum UserSettings{
        parkingCamera,
        suddenBrake,
        roadStripAlert,
        alertVolume,
        savedVideos,
        fpsCounter
    }


    public static HashMap<Integer, String> aiocaSettings;
    static {
        aiocaSettings = new HashMap<>();
        aiocaSettings.put(UserSettings.parkingCamera.ordinal(), "Camera");
        aiocaSettings.put(UserSettings.suddenBrake.ordinal(), "Sudden Brake Alarm");
        aiocaSettings.put(UserSettings.roadStripAlert.ordinal(), "Road Strip Alert");
    }

}
