package aioca.example.aiocaapplication.networking.observables;

import android.util.Log;

import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.InetAddress;

import aioca.example.aiocaapplication.networking.controllers.TcpController;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import io.reactivex.rxjava3.core.Single;

/**
 * http://reactivex.io/documentation/single.html
 */

public final class TcpSingle {

    private static String       TAG                     = "TcpSingleObservable";
    private AiocaNetworkService aiocaNetworkService;


    public TcpSingle(AiocaNetworkService aiocaNetworkService) {
    this.aiocaNetworkService = aiocaNetworkService;
    }

    /**
     * Create RXJava Single from a Callable which will send TCP Request with String Message
     * Will use the AiocaNetworkServices TCP Controller
     * @param message String: Message to Send
     * @param ctrl TcpController: TcpController to use to send Data
     * @param address InetAddress: InetAddress to send data to
     * @param port Int: Port to Send data on
     * @return RxJava Single: RXJava Single Observable
     */
    public Single<DatagramPacket> sendTcpRequest(String message, TcpController ctrl, InetAddress address, int port){
        return sendTcpRequest(new BigInteger(message, 16).toByteArray(), ctrl, address, port);
    }


    /**
     * Create RXJava Single from a Callable which will send TCP Request with byte[] Message
     * Will use the AiocaNetworkServices TCP Controller
     * @param message byte[]: Message to Send
     * @param ctrl TcpController: TcpController to use to send Data
     * @param address InetAddress: InetAddress to send data to
     * @param port Int: Port to Send data on
     * @return RxJava Single: RXJava Single Observable
     */
    public Single<DatagramPacket> sendTcpRequest(byte[] message, TcpController ctrl, InetAddress address, int port){
        return Single.fromCallable(() -> {
            /**
             * Setup TCP Sockets
             */
            try {
                ctrl.Setup(aiocaNetworkService.wiFiController.getConnectedNetwork(), address, port);
            } catch (Exception e) {
                e.printStackTrace();
            }

            /**
             * Transmit Message through AiocaNetworkService using TCP and Services TcpController
             */
            Log.d(TAG, "newSendTCPRequest: TCP Transmission Started");

            try {
                ctrl.Write(new DatagramPacket(message, message.length));
            }catch (Exception e){
                Log.w(TAG, "newSendTCPRequest: Error in Transmit", e);
            }

            byte[] receive = new byte[64];

            /**
             * Receive ack from, if nothing is received continue 20 times before closing sockets
             * and returning error on message sent
             */
            DatagramPacket received = new DatagramPacket(receive, receive.length);
            received.setLength(ctrl.Read(received));

            /**
             * Close TCP Sockets
             */
            ctrl.Destroy();

            return received; //Obejct size data numbers_read data
        });
    }
}
