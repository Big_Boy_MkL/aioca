package aioca.example.aiocaapplication.fragments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.Objects;

import aioca.example.aiocaapplication.adapter.SettingsAdapter;
import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.databinding.FragmentSettingsBinding;
import aioca.example.aiocaapplication.networking.observers.settingsobservers.SendSettingsObserver;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.util.ServiceHandle;
import aioca.example.aiocaapplication.viewmodels.SettingsViewModel;

/* Inspiration for Livedata in Fragments from
https://medium.com/mindorks/using-view-model-with-roomdatabase-95b790a9a66c */
public class SettingsFragment extends Fragment {

    private static String           TAG                 = "SettingsFragment";

    SettingsAdapter                 settingsAdapter;
    SettingsViewModel               settingsViewModel;

    boolean                         isBound             = false;
    AiocaNetworkService             aiocaNetworkService;

    FragmentSettingsBinding         settingsBinding;

    private ServiceHandle           serviceHandle;

    public SettingsFragment() {
    }

    /**
     * onCreate of SettingsFragment, will receive viewmodel, create the adapter, define the observer
     * on the livedata and define setClickListener for change of setting.
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serviceHandle = ServiceHandle.getInstance(Objects.requireNonNull(getActivity()).getApplicationContext());

        settingsViewModel = new ViewModelProvider(this).
                get(String.valueOf(R.string.Settings_ViewModel), SettingsViewModel.class);
        settingsAdapter = new SettingsAdapter(getContext());

        settingsViewModel.getSettingsListLiveData()
                .observe(this, settingsClasses ->
                        settingsAdapter.setSettingsList(settingsClasses));

        settingsAdapter.setClickListener(new SettingsAdapter.SettingsClickInterface() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onSettingClick(int position, SwitchMaterial switchMaterial) {
                SettingsClass settingsClass_ = Objects.requireNonNull(settingsViewModel.getSettingsListLiveData().getValue()).get(position);
                settingsClass_.setEnabled(!settingsClass_.isEnabled());

                aiocaNetworkService.sendUserSetting(new SendSettingsObserver(settingsViewModel, settingsClass_, switchMaterial), settingsClass_);
            }
        });
    }

    /**
     * onPause, Unbind from service
     */
    @Override
    public void onPause() {
        if (isBound){
            Log.d(TAG, "onPause: Unbinding from Service");
            serviceHandle.unbindToService(serviceConn);
        }
        isBound = false;
        super.onPause();
    }

    /**
     * onResume, will bind to service
     */
    @Override
    public void onResume() {
        if (!isBound){
            Log.d(TAG, "onResume: Binding to Service");

            if (!serviceHandle.bindToService(serviceConn)){
                Snackbar.make(Objects.requireNonNull(getView()),
                        "Cannot Connect to Service in onResume", Snackbar.LENGTH_LONG)
                        .show();
            }
        }
        super.onResume();
    }

    /**
     * onCreateView, when the view of the fragment is created,
     * will setup recyclerview and databinding
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        settingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        settingsBinding.setSettingsViewModel(settingsViewModel);


        if (!serviceHandle.bindToService(serviceConn)){
            Snackbar.make(Objects.requireNonNull(getView()),
                    "Cannot Connect to Service in onCreateView", Snackbar.LENGTH_LONG)
                    .show();
            Log.d(TAG, "onCreateView: Cannot bind to Service");
        }

        settingsBinding.settingsRecyclerview.setHasFixedSize(true);
        settingsBinding.settingsRecyclerview.setLayoutManager(new LinearLayoutManager(settingsBinding.getRoot().getContext()));
        settingsBinding.settingsRecyclerview.setAdapter(settingsAdapter);

        return settingsBinding.getRoot();
    }

    ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
            isBound = true;
            Log.d(TAG, "onServiceConnected: Connected to AiocaNetworkService");

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            aiocaNetworkService = null;
            isBound = false;
            Log.d(TAG, "onServiceDisconnected: Disconnected from AiocaNetworkService");

        }
    };

}