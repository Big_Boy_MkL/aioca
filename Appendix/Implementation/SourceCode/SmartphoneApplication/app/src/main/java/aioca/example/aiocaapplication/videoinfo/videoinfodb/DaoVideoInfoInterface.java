package aioca.example.aiocaapplication.videoinfo.videoinfodb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;

@Dao
public interface DaoVideoInfoInterface {

    @Query("SELECT * FROM VideoInfo")
    LiveData<List<VideoInfo>> getAllVideoInfoLive();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertVideoInfo(VideoInfo videoInfo);


}
