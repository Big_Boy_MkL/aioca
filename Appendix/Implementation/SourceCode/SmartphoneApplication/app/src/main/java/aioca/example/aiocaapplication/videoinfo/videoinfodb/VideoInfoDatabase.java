package aioca.example.aiocaapplication.videoinfo.videoinfodb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.videoinfo.models.TimeStampConverters;
import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;


/**
 * Room Database as singleton
 * https://developer.android.com/topic/libraries/architecture/room
 */
@Database(entities = VideoInfo.class, exportSchema = false, version = 1)
@TypeConverters({TimeStampConverters.class})
public abstract class VideoInfoDatabase extends RoomDatabase {

    private static VideoInfoDatabase db;

    public abstract DaoVideoInfoInterface daoVideoInfoInterface();

    /**
     * Return instance of the database
     * @param context application context
     * @return VideoInfoDatabase
     */
    public static synchronized VideoInfoDatabase getInstance(Context context){
        if (db == null){
            db = Room.databaseBuilder(context.getApplicationContext(), VideoInfoDatabase.class,
                    context.getString(R.string.videoinfo_db_string))
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return db;
    }
}
