package aioca.example.aiocaapplication.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;
import java.util.Objects;

import aioca.example.aiocaapplication.fragments.LoginFragment;
import aioca.example.aiocaapplication.settings.AiocaSettings;
import aioca.example.aiocaapplication.settings.executors.InsertSettingExecutor;
import aioca.example.aiocaapplication.settings.executors.UpdateSettingExecutor;
import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.settings.settingsdb.SettingsDatabase;
import aioca.example.aiocaapplication.settings.settingsdb.SettingsRepository;


/**
 * ViewModel for SettingsFragment
 * Inspiration taken from
 * https://developer.android.com/codelabs/android-training-livedata-viewmodel#14
 */
public class SettingsViewModel extends AndroidViewModel {

    private SettingsRepository settingsRepository;
    private LiveData<List<SettingsClass>> settingsLiveData;

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        settingsRepository = new SettingsRepository(application);
        settingsLiveData = settingsRepository.getAllSettings();
    }

    public LiveData<List<SettingsClass>> getSettingsListLiveData(){
        return settingsLiveData;
    }


    public void insertSetting(SettingsClass settingsClass){
        settingsRepository.insert(settingsClass);
    }
}
