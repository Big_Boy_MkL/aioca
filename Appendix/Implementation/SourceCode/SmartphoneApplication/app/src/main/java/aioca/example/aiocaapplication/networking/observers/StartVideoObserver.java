package aioca.example.aiocaapplication.networking.observers;

import android.content.Context;
import android.util.Log;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.IOException;
import java.net.DatagramPacket;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class StartVideoObserver implements SingleObserver<DatagramPacket> {

    AiocaNetworkService aiocaNetworkService;
    private final static String TAG = "StartVideoObserver";
    private Context context;

    public StartVideoObserver(AiocaNetworkService aiocaNetworkService, Context context) {
        this.aiocaNetworkService = aiocaNetworkService;
        this.context = context;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {
    }

    /**
     * Starts the UDP Video Feed when Observables succeeds
     * In this case when this Observer is used with a TcpSingle Request that succeeds
     * @param datagramPacket DatagramPacket: used if need to react to received input
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket)
    {
        Log.d(TAG, "onSuccess: Starting UDP Feed");

        try {
            aiocaNetworkService.startUDPFeed();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */
    @Override
    public void onError(@NonNull Throwable e) {
        /**
         * Simple Dialog Box that shows App cant connect to WiFi
         */
        new MaterialAlertDialogBuilder(context)
                .setTitle(context.getString(R.string.CameraErrorTitle))
                .setMessage(context.getString(R.string.CameraErrorMessage))
                .show();
    }

}
