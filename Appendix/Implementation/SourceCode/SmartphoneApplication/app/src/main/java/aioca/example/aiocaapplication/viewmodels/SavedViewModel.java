package aioca.example.aiocaapplication.viewmodels;

import android.app.Application;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.settings.settingsdb.SettingsRepository;
import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;
import aioca.example.aiocaapplication.videoinfo.videoinfodb.VideoInfoRepository;

/**
 * ViewModel for SavedVideos Fragment
 * Inspiration taken from
 * https://developer.android.com/codelabs/android-training-livedata-viewmodel#14
 */
public class SavedViewModel extends AndroidViewModel {


    private VideoInfoRepository videoInfoRepository;
    private LiveData<List<VideoInfo>> videoInfoLiveData;


    public SavedViewModel(@NonNull Application application) {
        super(application);
        videoInfoRepository = new VideoInfoRepository(application);
        videoInfoLiveData = videoInfoRepository.getVideoInfoLiveData();
    }

    public LiveData<List<VideoInfo>> getVideoInfoListLiveData(){
        return videoInfoLiveData;
    }


    public void insertSetting(VideoInfo videoInfo){
        videoInfoRepository.insert(videoInfo);
    }

}
