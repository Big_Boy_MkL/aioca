package aioca.example.aiocaapplication.videoinfo.executors;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.videoinfo.videoinfodb.DaoVideoInfoInterface;

/**
 * NOT USED
 * Executor for returning all video infos from the DB
 */
public class GetAllVideoInfoExecutor {

    DaoVideoInfoInterface daoVideoInfoInterface;

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2, 0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

    public GetAllVideoInfoExecutor(DaoVideoInfoInterface daoVideoInfoInterface){
        this.daoVideoInfoInterface = daoVideoInfoInterface;
    }

    public void getAllVideoInfoLiveData(){threadPoolExecutor.execute(getAllVideoInfoRunnable());}

    private Runnable getAllVideoInfoRunnable(){return () -> daoVideoInfoInterface.getAllVideoInfoLive();}
}
