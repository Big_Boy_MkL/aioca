package aioca.example.aiocaapplication.networking.controllers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.networking.callback.WiFiNetworkCallback;
import aioca.example.aiocaapplication.networking.sharedpreferences_handlers.NetworkIdSharedPreferences;

public class WiFiController {

    private final static String             TAG                     = "WiFiController";

    private String                          NETWORK_NAME;
    private String                          STANDARD_NETWORK_PASS;

    private ConnectivityManager             connMgr;
    private WifiManager                     wifiManager;
    WiFiNetworkCallback                     networkCallback;
    NetworkIdSharedPreferences              networkIdSharedPreferences;



    public WiFiController(String network_name,
                          String network_pass,
                          Context context,
                          ConnectivityManager connMgr,
                          WifiManager wifiManager) {

        this.NETWORK_NAME = network_name;
        this.STANDARD_NETWORK_PASS = network_pass;
        this.connMgr = connMgr;
        this.wifiManager = wifiManager;
        this.networkIdSharedPreferences = NetworkIdSharedPreferences.getInstance(context);

        this.networkCallback = new WiFiNetworkCallback(this.wifiManager);
    }

    /**
     * @return Boolean: Return whether WiFi Controller is connected to Network
     */
    public boolean isConnected() {
        return networkCallback.isConnected();
    }

    /**
     * @return Boolean: Returns the connected network, if none will return null
     */
    public Network getConnectedNetwork() {
        return networkCallback.getNetwork();
    }

    /**
     * Will register to the network with default parameters
     */
    public void registerToNetwork(){
        registerToNetwork(NETWORK_NAME, STANDARD_NETWORK_PASS);
    }


    /**
     * Will register to the network with the parameter SSID and parameter Password
     * This function is very much inspired from the Google Solution found in
     * https://issuetracker.google.com/issues/158718275
     * @param SSID string: Network SSID
     * @param password string: Password
     */
    public void registerToNetwork(String SSID, String password){
        if (connMgr != null) {
            try {
                connMgr.bindProcessToNetwork(null);
                connMgr.unregisterNetworkCallback(networkCallback);
            } catch (Exception e) {
                Log.d(TAG, "checkNetworkConnections: connMgr already registered");
            }
        }

        /**
         * NetworkRequest to Build a request to the correct network and
         * add a NetworkCallback
         */
        NetworkRequest.Builder builder = new NetworkRequest.Builder();
        builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
        builder.removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);


        Log.d(TAG, "checkNetworkConnections: Network Callback Registered");

        /**
         * Register the networkCallback to the Connection Manager
         */
        connMgr.registerNetworkCallback(builder.build(), networkCallback);

        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        /**
         * WifiConfiguration to Connect to Network
         * Deprecated in API 29
         */
        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.SSID = String.format("\"%s\"", SSID);
        wifiConfiguration.status = WifiConfiguration.Status.ENABLED;

        int netId = -1;

        List<WifiConfiguration> apList = wifiManager.getConfiguredNetworks();
        Log.d(TAG, "checkNetworkConnections: apList Size: " + apList.size());

        for (WifiConfiguration i : apList) {
            Log.d(TAG, "checkNetworkConnections: apList: " + i.SSID);
            if (i.SSID != null && i.SSID.equals("\"" + SSID + "\"")){
                netId = i.networkId;
            }
        }

        Log.d(TAG, "checkNetworkConnections: netID" + netId);
        if (netId == -1){
            if (TextUtils.isEmpty(password)) {
                Log.e(TAG, "====== Connect to open network");
                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            } else {
                Log.e(TAG, "====== Connect to secure network");
                wifiConfiguration.preSharedKey = String.format("\"%s\"", password);
            }

            netId = wifiManager.addNetwork(wifiConfiguration);

            networkIdSharedPreferences.setNetworkIdSharedPreferences(netId);

            Log.d(TAG, "checkNetworkConnections: Network Enabled " + netId);
            wifiManager.enableNetwork(netId, true);
        }
    }

    /**
     * connect to the WiFi given the saved WiFi Net ID in shared Preference
     * Should prob prompt user is fail to enable Network
     */
    public void connectToWiFiFromPause(){
        int netId = networkIdSharedPreferences.getNetworkIdSharedPreferences();

        if (netId != -1){
            wifiManager.enableNetwork(netId, true);
        }
    }

    /**
     * disconnect from the WiFi using the saved WiFi Net ID in shared Preference
     * Should prob promt user to fail to enable Network
     */
    public void disconnectFromWiFiOnPause(){
        int netId = networkIdSharedPreferences.getNetworkIdSharedPreferences();

        if (netId != -1){
            wifiManager.disableNetwork(netId);
        }
    }
}
