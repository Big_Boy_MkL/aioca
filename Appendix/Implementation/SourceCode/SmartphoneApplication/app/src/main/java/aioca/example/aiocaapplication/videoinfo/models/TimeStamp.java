package aioca.example.aiocaapplication.videoinfo.models;

/**
 * Model for TimeStamps in VideoInfo
 */
public class TimeStamp {

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    public TimeStamp(int year, int month, int day, int hour, int minute) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    public TimeStamp() {
        this.year = 1990;
        this.month = 1;
        this.day = 1;
        this.hour = 1;
        this.minute = 1;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }
}
