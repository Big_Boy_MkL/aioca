package aioca.example.aiocaapplication.videoinfo.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Model class for Video Infos in the Room Database
 */
@Entity
public class VideoInfo {

    @PrimaryKey
    @NonNull
    private TimeStamp timeStamp;

    @ColumnInfo(name = "startIdx")
    private int startIdx;

    @ColumnInfo(name = "size")
    private int size;

    @ColumnInfo(name = "n_images")
    private int n_images;


    public VideoInfo(int startIdx, int size, int n_images, @NonNull TimeStamp timeStamp) {
        this.startIdx = startIdx;
        this.size = size;
        this.n_images = n_images;
        this.timeStamp = timeStamp;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public void setStartIdx(int startIdx) {
        this.startIdx = startIdx;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getN_images() {
        return n_images;
    }

    public void setN_images(int n_images) {
        this.n_images = n_images;
    }

    @NonNull
    public TimeStamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(@NonNull TimeStamp timeStamp) {
        this.timeStamp = timeStamp;
    }

}
