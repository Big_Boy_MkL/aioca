package aioca.example.aiocaapplication.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.adapter.VideoInfoAdapter;
import aioca.example.aiocaapplication.databinding.FragmentSavedBinding;
import aioca.example.aiocaapplication.networking.observers.NumOfSavedVidObserver;
import aioca.example.aiocaapplication.networking.observers.RequestVideoObserver;
import aioca.example.aiocaapplication.networking.observers.VideoInfoObserver;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.util.ServiceHandle;
import aioca.example.aiocaapplication.videoencoder.InternalDataStorage;
import aioca.example.aiocaapplication.viewmodels.SavedViewModel;

public class SavedFragment extends Fragment {

    private static String TAG = "SavedFragment";

    private static int NUM_OF_COLUMNS = 5;

    VideoInfoAdapter videoInfoAdapter;

    FragmentSavedBinding fragmentSavedBinding;

    AiocaNetworkService aiocaNetworkService;
    SavedViewModel savedViewModel;
    InternalDataStorage storage;

    private ServiceHandle serviceHandle;
    private boolean downloadingVideo = false;

    public SavedFragment() {
    }

    /**
     * onCreate, will setup all objects needed to run save fragment
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.serviceHandle = ServiceHandle.getInstance(Objects.requireNonNull(getActivity()).getApplicationContext());

        storage = new InternalDataStorage(getActivity().getApplicationContext());
        savedViewModel = new ViewModelProvider(this)
                .get(String.valueOf(R.string.Saved_ViewModel), SavedViewModel.class);

        videoInfoAdapter = new VideoInfoAdapter(getContext());

        savedViewModel.getVideoInfoListLiveData()
                .observe(this, videoInfo ->
                        videoInfoAdapter.setVideoInfoList(videoInfo));

        videoInfoAdapter.setClickListener((position, v) -> {
            buttonPressed(position);
        });

    }

    /**
     * onCreateView, when the view of the fragment is created,
     * will setup recyclerview and databinding
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentSavedBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_saved, container, false);

        if (downloadingVideo){
            fragmentSavedBinding.downloadVideoProgress.setVisibility(View.VISIBLE);
            fragmentSavedBinding.savedRecyclerview.setVisibility(View.GONE);
        }else {
            fragmentSavedBinding.downloadVideoProgress.setVisibility(View.GONE);
            fragmentSavedBinding.savedRecyclerview.setVisibility(View.VISIBLE);
        }

        fragmentSavedBinding.setSavedViewModel(savedViewModel);


        if (!serviceHandle.bindToService(serviceConn)){
            Snackbar.make(Objects.requireNonNull(getView()),
                    "Cannot Connect to Service in onCreateView", Snackbar.LENGTH_LONG)
                    .show();
            Log.d(TAG, "onCreateView: Cannot bind to Service");
        }

        fragmentSavedBinding.savedRecyclerview.setHasFixedSize(true);
        fragmentSavedBinding.savedRecyclerview.setLayoutManager(new GridLayoutManager(fragmentSavedBinding.getRoot().getContext(), NUM_OF_COLUMNS));
        fragmentSavedBinding.savedRecyclerview.setAdapter(videoInfoAdapter);

        return fragmentSavedBinding.getRoot();
    }


    ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
            aiocaNetworkService.requestNumOfVid(new NumOfSavedVidObserver(this::GetVideoInfo));
            Log.d(TAG, "onServiceConnected: Connected to AiocaNetworkService");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            aiocaNetworkService = null;
            Log.d(TAG, "onServiceDisconnected: Disconnected from AiocaNetworkService");

        }

        /**
         * Will create numberOfSavedVideos RxJava request to receive info about it
         * @param numberOfSavedVideos int number of videos to receive
         */
        private void GetVideoInfo(int numberOfSavedVideos)
        {
            for (int i = 0; i < numberOfSavedVideos; i++) {
                aiocaNetworkService.requestVideoInfo(new VideoInfoObserver(i, savedViewModel), i);
            }
        }
    };

    /**
     * Function for what needs to happen when a video is pressed
     * @param index int index of what video is pressed
     */
    private void buttonPressed(int index)
    {
        if (!downloadingVideo) {
            Log.d(TAG, "buttonPressed: ");
            if (storage.VideoExists(index)) {
                ShowVideo(index);
            }
            else {
                downloadingVideo = true;
                RequestVideoObserver req = new RequestVideoObserver(this::resetDownloadVideo, aiocaNetworkService,
                        Objects.requireNonNull(getActivity()).getApplicationContext(), index, fragmentSavedBinding.downloadVideoProgress);

                aiocaNetworkService.requestVideo(req, index);

                fragmentSavedBinding.savedRecyclerview.setVisibility(View.GONE);
                fragmentSavedBinding.downloadVideoProgress.setVisibility(View.VISIBLE);


            }
        }
    }

    /**
     * Will reset the view of the video.
     */
    private void resetDownloadVideo()
    {
        fragmentSavedBinding.downloadVideoProgress.setVisibility(View.GONE);
        fragmentSavedBinding.downloadVideoProgress.setProgress(0);
        fragmentSavedBinding.savedRecyclerview.setVisibility(View.VISIBLE);

        downloadingVideo = false;
    }

    /**
     * Will start a new Activity with the video player
     * @param videoNumber int what video number to open
     */
    private void ShowVideo(int videoNumber){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(storage.GetPath(videoNumber).getPath()), "video/*");
        startActivity(intent);
    }
}