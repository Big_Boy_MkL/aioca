package aioca.example.aiocaapplication.settings.settingsdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import aioca.example.aiocaapplication.settings.SettingsClass;

@Dao
public interface DaoSettingsInterface {


    @Query("SELECT * FROM SettingsClass")
    LiveData<List<SettingsClass>> getAllLive();

    @Query("SELECT * FROM SettingsClass")
    List<SettingsClass>getAllList();

    @Query("SELECT * FROM SettingsClass WHERE name = :name")
    SettingsClass getSetting(String name);

    //@Insert
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSetting(SettingsClass settingsClass);

    @Delete
    void removeSetting(SettingsClass settingsClass);

    @Update
    void update(SettingsClass... word_);



}
