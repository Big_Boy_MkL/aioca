package aioca.example.aiocaapplication.videoinfo.models;

import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * https://developer.android.com/reference/androidx/room/TypeConverters
 * https://howtodoinjava.com/java/date-time/format-localdatetime-to-string/
 * Type Converters for converting TimeStamp Objects into String and Back
 */
public class TimeStampConverters {

    private static String datePattern = "MM:dd:yyyy:HH:mm";

    /**
     * @param val String following pattern
     * @return TimeStamp Object
     */
    @TypeConverter
    public static TimeStamp fromStringToTimeStamp(String val){

        int firstSep = datePattern.indexOf(':');
        int secondSep = datePattern.indexOf(':', firstSep+1);
        int thirdSep = datePattern.indexOf(':', secondSep+1);
        int fourthSep = datePattern.indexOf(':', thirdSep+1);

        int month = Integer.parseInt(val.substring(0, firstSep));
        int day = Integer.parseInt(val.substring(firstSep+1, secondSep));
        int year = Integer.parseInt(val.substring(secondSep+1, thirdSep));
        int hour = Integer.parseInt(val.substring(thirdSep+1, fourthSep));
        int minutes = Integer.parseInt(val.substring(fourthSep+1));
        return new TimeStamp(year, month, day, hour, minutes);
    }

    /**
     * @param timeStamp Timestamp to upload
     * @return a string representation following the pattern
     */
    @TypeConverter
    public static String fromTimeStampToString(TimeStamp timeStamp){
        LocalDateTime localDateTime = LocalDateTime.of(timeStamp.getYear(), timeStamp.getMonth(),
                timeStamp.getDay(), timeStamp.getHour(), timeStamp.getMinute());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern);

        return localDateTime.format(formatter);
    }


}
