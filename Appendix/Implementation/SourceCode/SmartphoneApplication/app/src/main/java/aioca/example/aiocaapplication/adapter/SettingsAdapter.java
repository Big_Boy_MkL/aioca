package aioca.example.aiocaapplication.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.List;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.settings.SettingsClass;

/**
 * Inspiration Taken from:
 * https://developer.android.com/guide/topics/ui/layout/recyclerview
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.settingsViewHolder>{


    private List<SettingsClass> settingsArrayList;
    private SettingsClickInterface settingsClickInterface;
    private Context mContext;

    public SettingsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setClickListener(SettingsClickInterface clickListener){
        settingsClickInterface = clickListener;
    }

    /**
     * Will create a settingsViewHolder for context and add inflate the cardview and send the Click
     * Interface.
     * @param parent ViewGroup
     * @param viewType int
     * @return SettingsViewHolder
     */
    @NonNull
    @Override
    public SettingsAdapter.settingsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new settingsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.settings_card_view, parent, false),
                settingsClickInterface);
    }

    /**
     * What will happen on each bind for each card.
     * @param holder what settingsViewHolder to use
     * @param position int: position in the recyclerview
     */
    @Override
    public void onBindViewHolder(@NonNull SettingsAdapter.settingsViewHolder holder, int position) {

        String name_ = settingsArrayList.get(position).getName();
        holder.txtName.setText(name_);
        holder.aSwitch.setChecked(settingsArrayList.get(position).isEnabled());

        String lower_case_name = name_.toLowerCase();
        lower_case_name = lower_case_name.replace(" ", "_");

        Resources res = mContext.getResources();
        Drawable im_res;
        try{
            im_res = ResourcesCompat.getDrawable(res,
                    res.getIdentifier(lower_case_name, "drawable", mContext.getPackageName())
                    , null);
        }catch (Resources.NotFoundException e){
            im_res = ResourcesCompat.getDrawable(res, R.drawable.settings, null);
        }

        holder.imageView.setImageDrawable(im_res);

    }

    /**
     * Set the arraylist of the adapter and notify the recyclerview that it has changed
     * @param settingsArrayList list of settingclasses
     */
    public void setSettingsList(List<SettingsClass> settingsArrayList) {
        this.settingsArrayList = settingsArrayList;
        notifyDataSetChanged();
    }

    /**
     * Get the number of items in the recyclerview
     * @return int, number of items
     */
    @Override
    public int getItemCount() {
        if (settingsArrayList != null)
        {
            return settingsArrayList.size();
        }
        return 0;
    }

    public interface SettingsClickInterface{
        void onItemClick(int position, View v);
        void onSettingClick(int position, SwitchMaterial v);
    }

    public static class settingsViewHolder extends RecyclerView.ViewHolder{
        private SettingsClickInterface settingsClickInterface;
        private TextView txtName;
        private SwitchMaterial aSwitch;
        private ImageView imageView;

        /**
         * What the settingsViewHolder will contain aswell as bind the ClickInterface to buttons
         * @param itemView View
         * @param settingsClickInterface SettingsClickInterface, methods of the interface.
         */
        public settingsViewHolder(@NonNull View itemView, final SettingsClickInterface settingsClickInterface) {
            super(itemView);
            this.settingsClickInterface = settingsClickInterface;
            txtName = itemView.findViewById(R.id.card_view_name);
            aSwitch = itemView.findViewById(R.id.card_view_switch);
            imageView = itemView.findViewById(R.id.card_view_settings_icon);

            itemView.setOnClickListener(v -> {
                if (settingsClickInterface != null){
                    settingsClickInterface.onItemClick(getLayoutPosition(), v);
                }
            });
            aSwitch.setOnClickListener(v -> {
                if (settingsClickInterface != null){
                int position = getAdapterPosition();
                settingsClickInterface.onSettingClick(position, aSwitch);
                }
            });

        }
    }


}
