#ifndef _TCPSCOKET_H_
#define _TCPSCOKET_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <ISocket.hpp>
#include <PacketHandler.hpp>
#include <TimeStamp.hpp>

namespace TCP
{
    class Socket : public ISocket
    {
    public:
        Socket(Utility::ITimeStamp* time, QueueHandle_t msgQueue, PacketHandler* phandler);
        ~Socket();

        /**
        * \brief Starts the socket task and starts the packethandler task. 
        */
        void Start();

        /**
        * \brief Stops the socket task and stops the packethandler task. 
        */
        void Stop();  

        /**
        * \brief Task function that is used to continuously listen for
        *        new connections. If a connection is establied a task
        *        running \ref CheckMessageReceived will be started. 
        */
        void listenSocket();

        /**
        * \brief This should not be called! Its called through a task wrapper.
        *        Checks if a messeage is received withing 2 seconds. 
        *        If a message is recieved it's put in the \ref messageQueue
        *        so it can be handled by the packetHandler. 
        * 
        * \param[in] sock       Child socket id used to read message. 
        */
        void CheckMessageReceived(int sock);

        /**
        * \brief Sends a message to a child socket. 
        *
        * \param[in] sock       Child socket id to send message to.
        * \param[in] message    Pointer to the message.
        * \param[in] len        Length of message to be sent.
        * \param[in] done       Boolean to determinde if child socket should be shutdown after message.
        */
        void SendMessage(int* sock, uint8_t* message, int len, bool done);

        /**
        * \brief Sends a error to a child socket. 
        *
        * \param[in] sock       Child socket id to send message to.
        * \param[in] error      Error command which can be found in xxxx.
        */
        void SendError(int* sock, uint8_t error);

    private:
        /**
         * Variable to hold socket id. 
         */
        int listen_sock_ = 0;

        /**
         * Struct to hold number of max seconds to wait for a message
         */
        struct timeval tv_ = {
            .tv_sec = 5,
            .tv_usec = 0,
        };

        /**
         * Timestamp object to be able to check timeout
         */
        Utility::ITimeStamp* timeStamper_;

        /**
         * Handle for queue. 
         */
        QueueHandle_t messageQueue_;

        /**
         * Variable to hold socket id. 
         */
        PacketHandler* pkgHandler_;

        /**
         * Handles for tasks. 
         */
        TaskHandle_t * listenTaskHandle_ = NULL;
        TaskHandle_t * packetTaskHandle_ = NULL;

        /**
         * \brief Recevies a message on a socket and puts the message in a queue
         * 
         * \param[in] sock       Child socket id to receive message from.
         */
        void ReceiveMessage(int* sock);

        /**
         * \brief Shuts down a child socket
         * 
         * \param[in] sock       Child socket id to shutdown.
         */
        void Shutdown(int* sock);
    };
}

#endif