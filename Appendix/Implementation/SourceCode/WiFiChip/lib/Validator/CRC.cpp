/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "esp32/rom/crc.h"

/**
 * Private Headers
 */
#include "CRC.hpp"

namespace Validator
{
    CRC::CRC()
    {
    }

    CRC::~CRC()
    {
    }

    bool CRC::Validate(uint8_t *message, int* length)
    {
        // Calculate reverse CRC
        uint16_t CRC = ~crc16_be((uint16_t)~0xffff, message, *length);

        // Return
        return (CRC == 0) ? true : false;
    }

    uint16_t CRC::GetValdidationCode(uint8_t *message, int* length)
    {
        // Calculate CRC
        uint16_t CRC = ~crc16_be((uint16_t)~0xffff, message, *length);

        // Return CRC
        return CRC;
    }
} // namespace Validator