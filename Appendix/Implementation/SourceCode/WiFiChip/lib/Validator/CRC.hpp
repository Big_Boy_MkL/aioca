#ifndef _CRC_H_
#define _CRC_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "IValidator.hpp"

namespace Validator
{
    class CRC : public IValidator
    {
    public:
        CRC();
        ~CRC();
    
        /**
        * \brief Checks if the CRC on a message is valid or not. 
        *
        * \param[in] message    A uint8_t array containing the message.
        * \param[in] length     Length of the message.
        * \return               Returns true message is ok and false if error. 
        */
        bool Validate(uint8_t *message, int* length);

        /**
        * \brief Calcualtes a crc for a message. 
        *
        * \param[in] message    A uint8_t array containing the message.
        * \param[in] length     Length of the message.
        * \return               uint16_t CRC value. 
        */
        uint16_t GetValdidationCode(uint8_t *message, int* length);
    };
} // namespace Validator

#endif