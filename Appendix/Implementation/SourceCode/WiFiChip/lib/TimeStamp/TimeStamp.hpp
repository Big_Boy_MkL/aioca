#ifndef _TIMESTAMP_H_
#define _TIMESTAMP_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "driver/timer.h"

/**
 * Private Headers
 */
#include <ISocket.hpp>
#include <ITimeStamp.hpp>

namespace Utility
{
    class TimeStamp : public ITimeStamp
    {
    public:
        TimeStamp(timer_group_t group, timer_idx_t index);
        ~TimeStamp();

        /**
        * \brief Adds a stamped time to message. 
        *
        * \param[in] data       A \ref MessageQueueData struct containing message.
        */
        void AddTimeStamp(MessageQueueData *data);

        /**
        * \brief Compares stored stamped time and current stamped time. 
        *
        * \param[in] data       A \ref MessageQueueData struct containing message.
        * \return               Returns true if timeout has not been reached. 
        */
        bool CompareTimeStamp(MessageQueueData *data);

    private:
        /**
         * Group number for the timer that should be used. 
         */
        timer_group_t group_;

        /**
         * Index for the timer that should be used. 
         */
        timer_idx_t index_;
    };
} // namespace Utility
#endif