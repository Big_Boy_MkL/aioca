/**
 * C++ Libs
 */
#include <cstring>

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <Communication.hpp>

namespace STM32
{
    Communication::Communication(Validator::IValidator *val, Bus::IBUS *bus) : val_(val), bus_(bus)
    {
    }

    Communication::~Communication()
    {
    }

    void Communication::WriteMessage(uint8_t *message, int *length)
    {
        // Check if message length is too big
        if (*length > COMMUNICATION_MAX_TRANSFER)
        {
            throw PROTOCOL_ERROR_MAXTRANSFER_EXCEEDED;
        }

        // Store message pointers so its possible to resend same msg
        orgMsg_ = message;
        orgLen_ = length;

        // Add CRC size to length and create send buffer
        int len = *length + 2;
        uint8_t buffer[SPI_MAX_TRANSFER];

        // Get validation
        uint16_t CRC = val_->GetValdidationCode(message, length);

        // Copy msg to transmit buffer
        memcpy(buffer, message, *length);

        // Append CRC to msg
        buffer[len - 2] = CRC >> 8;
        buffer[len - 1] = (uint8_t)CRC;

        // Write to BUS
        bus_->WriteMessage(buffer, &len);
    }

    void Communication::ReceiveMessage(uint8_t *message, int *length)
    {
        // Enter loop te be able to retry sending msg
        for (size_t i = 0; i < COMMUNICATION_RETRIES; i++)
        {
            // Check if message is to big
            if (*length <= COMMUNICATION_MAX_TRANSFER)
            {
                // Add CRC size to length
                int len = *length + 2;
                
                // Read message from BUS
                bus_->ReadMessage(message, &len);

                // Check if message is valid
                if (val_->Validate(message, &len))
                {
                    return;
                }
                else
                {
                    // Check if message should be resent
                    if (i < COMMUNICATION_RETRIES - 1)
                    {
                        // Resend orginal msg with a delay
                        WriteMessage(orgMsg_, orgLen_);
                        vTaskDelay(RETRY_DELAY / portTICK_PERIOD_MS);
                    }
                }
            }
            else
            {
                throw PROTOCOL_ERROR_MAXTRANSFER_EXCEEDED;
            }
        }
        throw PROTOCOL_ERROR_MAINCTRLRESPONSEINVALID;
    }

    void Communication::SendACK()
    {
        // Create buffer with ACK msg
        int length = 3;
        uint8_t txBuffer[length] = {0x01, PROTOCOL_ACK, 0x04};

        // Send data
        WriteMessage(txBuffer, &length);
    }

    void Communication::SendNAK()
    {
        // Create buffer with ACK msg
        int length = 3;
        uint8_t txBuffer[length] = {0x01, PROTOCOL_ACK, 0x04};

        // Send data
        WriteMessage(txBuffer, &length);
    }
} // namespace STM32