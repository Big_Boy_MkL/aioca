/**
 * C++ Libs
 */
#include <cstring>

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "esp_log.h"
#include "driver/spi_master.h"

/**
 * Private Headers
 */
#include <SPI.hpp>
#include <GlobalDefines.hpp>

namespace Bus
{
    SPI::SPI(spi_host_device_t device) : hostDevice_(device)
    {
        // Add device to bus
        AddDevice(SPI_MOSI_GPIO_PIN, SPI_MISO_GPIO_PIN, SPI_SCLK_GPIO_PIN, SPI_CS_GPIO_PIN, SPI_MAX_TRANSFER);
    }

    /**************************************************************************************************/

    SPI::~SPI()
    {
        // Free spi
        ESP_ERROR_CHECK(spi_bus_remove_device(spiHandle_));
        ESP_ERROR_CHECK(spi_bus_free(hostDevice_));
    }

    /**************************************************************************************************/

    void SPI::AddDevice(int mosi_io_num, int miso_io_num, int sclk_io_num, int cs_io_num, int max_transfer_sz)
    {
        // Create config struckt
        spi_bus_config_t config = spi_bus_config_t();

        // Setup config
        config.mosi_io_num = mosi_io_num;
        config.miso_io_num = miso_io_num;
        config.sclk_io_num = sclk_io_num;
        config.quadwp_io_num = -1;
        config.quadhd_io_num = -1;
        config.max_transfer_sz = max_transfer_sz;

        // Initialize spi with config
        ESP_ERROR_CHECK(spi_bus_initialize(hostDevice_, &config, 0)); //TODO måske opsæt DMA

        // Create interface config
        spi_device_interface_config_t devcfg = spi_device_interface_config_t(); // TODO Split den her op i mindre functioner!

        // Setup spi interface
        devcfg.clock_speed_hz = SPI_CLOCK_FREQ;
        devcfg.mode = SPI_MODE;
        devcfg.command_bits = 0;
        devcfg.address_bits = 0;
        devcfg.dummy_bits = 0;
        devcfg.spics_io_num = cs_io_num;
        devcfg.queue_size = 1;

        // Add spi to bus (Its possible to run multiple interfaces on same bus)
        ESP_ERROR_CHECK(spi_bus_add_device(hostDevice_, &devcfg, &spiHandle_));
    }

    /**************************************************************************************************/

    bool SPI::WriteMessage(const uint8_t *message, int *length)
    {
        if (*length <= SPI_MAX_TRANSFER)
        {
            // Create transaction
            spi_transaction_t data = spi_transaction_t();
            data.length = 8 * *length;
            data.tx_buffer = message;
            data.rx_buffer = NULL;

            // Send msg
            ESP_ERROR_CHECK(spi_device_transmit(spiHandle_, &data));

            return true;
        }
        return false;
    }

    /**************************************************************************************************/

    bool SPI::ReadMessage(uint8_t *receiveData, int *length)
    {
        if (*length <= SPI_MAX_TRANSFER)
        {
            // Create transaction
            spi_transaction_t data = spi_transaction_t();
            data.length = 8 * *length;
            data.rxlength = 8 * *length;
            data.tx_buffer = NULL;
            data.rx_buffer = receiveData;

            // Receive msg
            ESP_ERROR_CHECK(spi_device_transmit(spiHandle_, &data));

            return true;
        }
        return false;
    }
} // namespace Bus