/**
 * C++ Libs
 */
#include <cstring>

/**
 * FreeRTOS Libs
 */
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <PacketHandler.hpp>
#include <Socket.hpp>
#include <TimeStamp.hpp>
#include <GlobalDefines.hpp>

PacketHandler::PacketHandler(QueueHandle_t queue, Utility::ITimeStamp *stamp, STM32::ICommunication *comm) : queue_(queue), timeStamperPtr_(stamp), comm_(comm), currentCommand_(0), currentDelay_(0)
{
}

/**************************************************************************************************/

PacketHandler::~PacketHandler()
{
}

/**************************************************************************************************/

void PacketHandler::Handle()
{
    // Create struckt for data
    MessageQueueData data;

    // Get message from queue
    if (xQueueReceive(queue_, &(data), (TickType_t)0xff) == pdPASS)
    {
        // Validate message
        if (ValidateMsg(&data))
        {
            // Parse message and handle
            HandleReceivedMessage(&data);
        }
        else
        {
            // Send error
            data.socketPtr->SendError(&data.sock, PROTOCOL_ERROR_MESSAGEINVALID);
        }

        // Delete message
        delete[] data.message;
    }
}

/**************************************************************************************************/

void PacketHandler::HandleReceivedMessage(MessageQueueData *msgData)
{
    try
    {
        // Forward request
        comm_->WriteMessage(msgData->message, &msgData->len);
    }
    catch (const int error)
    {
        // Send error
        msgData->socketPtr->SendError(&msgData->sock, error);
        return;
    }

    // Create pointer for receive buffer
    uint8_t rxBuffer[COMMUNICATION_MAX_TRANSFER + 2];
    vTaskDelay(currentDelay_ / portTICK_PERIOD_MS);
    StandardRequest(rxBuffer, msgData);
}

/**************************************************************************************************/

void PacketHandler::StandardRequest(uint8_t *rxBuffer, MessageQueueData *msgData)
{
    // Find correct response length
    int rxBufferLength = FindResponseLength(msgData);

    // Check timeout
    if (timeStamperPtr_->CompareTimeStamp(msgData))
    {
        try
        {
            // Receive message on serial BUS
            comm_->ReceiveMessage(rxBuffer, &rxBufferLength);
        }
        catch (const int error)
        {
            // Send error
            msgData->socketPtr->SendError(&msgData->sock, error);
            return;
        }

        // Forward response to network socket
        msgData->socketPtr->SendMessage(&msgData->sock, rxBuffer, rxBufferLength, true);
    }
}

/**************************************************************************************************/

bool PacketHandler::ValidateMsg(MessageQueueData *data)
{   
    // Check if message contains start and stop bytes
    if (data->message[0] != PROTOCOL_START || data->message[data->len - 1] != PROTOCOL_END)
    {
        return false;
    }

    for (size_t i = 0; i < sizeof(ProtocolInfo_.protocolCommands) / sizeof(ProtocolInfo_.protocolCommands[0]); i++)
    {
        if (data->message[2] == ProtocolInfo_.protocolCommands[i])
        {
            currentCommand_ = data->message[2];
            currentDelay_ = RETRY_DELAY;
        }
        else
        {
            currentDelay_ = DELAY;
        }
    }

    return true;
}

/**************************************************************************************************/

int PacketHandler::FindResponseLength(MessageQueueData *data)
{
    if(data->message[1] == PROTOCOL_ACK || data->message[1] == PROTOCOL_NAK)
    {
        if(currentCommand_ == PROTOCOL_VD_R)
        {
            return COMMUNICATION_MAX_TRANSFER;
        }
        else
        {
            return 3;
        }
    }

    // Go though command array to find response size
    for (size_t i = 0; i < sizeof(ProtocolInfo_.protocolCommands) / sizeof(ProtocolInfo_.protocolCommands[0]); i++)
    {
        if (data->message[2] == ProtocolInfo_.protocolCommands[i])
        {
            return ProtocolInfo_.protocolReceiveLength[i];
        }
    }
    return 0;
}