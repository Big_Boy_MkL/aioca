#ifndef _IPACKETHANDLER_H_
#define _IPACKETHANDLER_H_

class IPacketHandler
{   
public:
    /**
     * \brief This should not be called from other than task wrappers.
     *        Starts handling messeges that is put into TCPSockets 
     *        \ref messageQueue. 
     */
    virtual void Handle() = 0;
};

#endif