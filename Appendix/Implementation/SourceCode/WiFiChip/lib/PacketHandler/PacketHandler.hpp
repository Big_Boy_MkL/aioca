#ifndef _PACKETHANDLER_H_
#define _PACKETHANDLER_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <ISocket.hpp>
#include <TimeStamp.hpp>
#include <Communication.hpp>

class PacketHandler
{   
public:
    PacketHandler(QueueHandle_t queue, Utility::ITimeStamp *stamp, STM32::ICommunication* comm);
    ~PacketHandler();

    /**
     * \brief This should not be called from other than task wrappers.
     *  Starts handling messeges that is put into TCPSockets 
     *  \ref messageQueue. 
     */
    void Handle();

private:
    /**
     * Handle for message queue. 
     */
    QueueHandle_t queue_;

    /**
     * Pointer to TimeStamp object so its possible to 
     * check timeout. 
     */
    Utility::ITimeStamp* timeStamperPtr_;

    /**
     * SpiDrive object used to send messages with SPI
     */
    STM32::ICommunication* comm_;

    /**
     * Stores the current command. This is done to make a 
     * video request run faster by lowering the delays. 
     */
    uint8_t currentCommand_;

    /**
     * Current delay between forwarding the request and requesting the response
     */
    uint16_t currentDelay_;

    /**
    * \brief Handels a received message. 
    *
    * \param[in] data       A \ref MessageQueueData struct containing message.
    */
    void HandleReceivedMessage(MessageQueueData* data);

    /**
    * \brief Handles a standad request. 
    *
    * \param[in] msgData    A \ref MessageQueueData struct containing message.
    * \param[in] rxBuffer   A uint8_t array containing the receive buffer.
    * \return               Returns true if timeout has not been reached. 
    */
    void StandardRequest(uint8_t* rxBuffer, MessageQueueData* msgData);

    /**
    * \brief Validates the received message. 
    *
    * \param[in] data       A \ref MessageQueueData struct containing message.
    * \return               Returns true if the message is okay else false. 
    */
    bool ValidateMsg(MessageQueueData* data);

    /**
    * \brief Finds the correct length of the response to the command. 
    *
    * \param[in] data       A \ref MessageQueueData struct containing message.
    * \return               Returns length of response. 
    */
    int FindResponseLength(MessageQueueData *data);
};

#endif