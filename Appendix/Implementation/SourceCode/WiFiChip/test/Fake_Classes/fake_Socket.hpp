#ifndef _FAKE_SOCKET_H_
#define _FAKE_SOCKET_H_

#include <ISocket.hpp>
#include <cstring>

namespace TCP
{
    class fake_socket : public ISocket
    {
    public:
        void Start(){}        
        void Stop(){}
        void listenSocket(){}
        void CheckMessageReceived(int sock)
        {}
        void SendMessage(int *sock, uint8_t *message, int len, bool done)
        {
            memcpy(message_, message, len);
            len_ = len;
        }
        void SendError(int *sock, uint8_t error)
        {
            error_ = error;
        }

        uint8_t* ReturnMessage()
        {
            return message_;
        }

        int ReturnLen()
        {
            return len_;
        }

        uint8_t ReturnError()
        {
            return error_;
        }

    private:
        uint8_t message_[64];
        int len_;
        uint8_t error_;
        
    };
} // namespace TCP

#endif