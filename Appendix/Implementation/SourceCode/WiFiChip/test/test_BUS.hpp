#ifndef _TEST_BUS_H_
#define _TEST_BUS_H_

#include <unity.h>
#include <SPI.hpp>

void test_WriteMessage_Pass()
{
    auto bus = Bus::SPI(SPI2_HOST);

    int len = 4;
    uint8_t message[len] = {0x01, 0x02, 0x03, 0x04};

    TEST_ASSERT_EQUAL(true, bus.WriteMessage(message, &len));
}

void test_WriteMessage_Error()
{
    auto bus = Bus::SPI(SPI2_HOST);

    int len = 100;
    uint8_t message[len];

    TEST_ASSERT_EQUAL(false, bus.WriteMessage(message, &len));
}

void test_ReadMessage()
{
    auto bus = Bus::SPI(SPI2_HOST);

    int len = 2;
    uint8_t message[len];
    bus.ReadMessage(message, &len);

    for (size_t i = 0; i < len; i++)
    {
        TEST_ASSERT_EQUAL(0x00, message[i]);
    }
}

void test_BUS()
{
    RUN_TEST(test_ReadMessage);
    RUN_TEST(test_WriteMessage_Pass);
    RUN_TEST(test_WriteMessage_Error);
}

#endif