#ifndef _TEST_COMMUNICATION_H_
#define _TEST_COMMUNICATION_H_

#include <unity.h>
#include <GlobalDefines.hpp>
#include <Communication.hpp>
#include "Fake_Classes/fake_BUS.hpp"
#include "Fake_Classes/fake_Validator.hpp"

void test_CommWriteMessage_Pass()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeVal.setGetValdidationCodeReturn(0xffff, 0);
    fakeBus.setGetWriteMessageCodeReturn(true, 0);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(false, exeptionThrown);
    TEST_ASSERT_EQUAL(1, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(1, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommWriteMessage_TooLongMsg()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup test message
    bool exeptionThrown = false;
    int len = 100;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(true, exeptionThrown);
    TEST_ASSERT_EQUAL(0, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(0, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommReceiveMessage_Pass()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(true, 0);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(false, exeptionThrown);
    TEST_ASSERT_EQUAL(1, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(1, fakeVal.ValidateNumOfCalls());
}

void test_CommReceiveMessage_TooLongMsg()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup test message
    bool exeptionThrown = false;
    int len = 100;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(true, exeptionThrown);
    TEST_ASSERT_EQUAL(0, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(0, fakeVal.ValidateNumOfCalls());
}

void test_CommReceiveMessage_OneValidateError()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(false, 0);
    fakeVal.setValidateReturn(true, 1);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message,&len);
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(false, exeptionThrown);
    TEST_ASSERT_EQUAL(2, fakeVal.ValdidationCodeNumOfCalls());
    TEST_ASSERT_EQUAL(2, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(2, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(2, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommReceiveMessage_TwoValidateError()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(false, 0);
    fakeVal.setValidateReturn(false, 1);
    fakeVal.setValidateReturn(true, 2);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message,&len);
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(false, exeptionThrown);
    TEST_ASSERT_EQUAL(3, fakeVal.ValdidationCodeNumOfCalls());
    TEST_ASSERT_EQUAL(3, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(3, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(3, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommReceiveMessage_FiveValidateError()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(false, 0);
    fakeVal.setValidateReturn(false, 1);
    fakeVal.setValidateReturn(false, 2);
    fakeVal.setValidateReturn(false, 3);
    fakeVal.setValidateReturn(false, 4);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message,&len);
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(true, exeptionThrown);
    TEST_ASSERT_EQUAL(5, fakeVal.ValdidationCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommReceiveVideo_Pass()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(true, 0);
    bool exeptionThrown = false;

    //Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(false, exeptionThrown);
    TEST_ASSERT_EQUAL(1, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(0, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(1, fakeVal.ValidateNumOfCalls());
}

void test_CommReceiveVideo_FiveValidateError()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    fakeBus.setGetReadMessageCodeReturn(true, 0);
    fakeVal.setValidateReturn(false, 0);
    fakeVal.setValidateReturn(false, 1);
    fakeVal.setValidateReturn(false, 2);
    fakeVal.setValidateReturn(false, 3);
    fakeVal.setValidateReturn(false, 4);
    bool exeptionThrown = false;

    // Setup test message
    int len = 10;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.WriteMessage(message,&len);
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(true, exeptionThrown);
    TEST_ASSERT_EQUAL(5, fakeVal.ValdidationCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeBus.WriteMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(5, fakeVal.ValdidationCodeNumOfCalls());
}

void test_CommReceiveVideo_MsgTooLong()
{
    // Setup fake modules and module being tested
    fake_Test_Validator fakeVal = fake_Test_Validator();
    fake_Test_BUS fakeBus = fake_Test_BUS();
    STM32::Communication comm = STM32::Communication(&fakeVal, &fakeBus);

    // Setup return values
    bool exeptionThrown = false;

    //Setup test message
    int len = 100;
    uint8_t message[len];

    try
    {
        // Simulate call
        comm.ReceiveMessage(message, &len);
    }
    catch (...)
    {
        exeptionThrown = true;
    }

    // Assert
    TEST_ASSERT_EQUAL(true, exeptionThrown);
    TEST_ASSERT_EQUAL(0, fakeBus.ReadMessageCodeNumOfCalls());
    TEST_ASSERT_EQUAL(0, fakeVal.ValidateNumOfCalls());
}

void test_COM()
{
    RUN_TEST(test_CommWriteMessage_Pass);
    RUN_TEST(test_CommWriteMessage_TooLongMsg);
    RUN_TEST(test_CommReceiveMessage_Pass);
    RUN_TEST(test_CommReceiveMessage_TooLongMsg);
    RUN_TEST(test_CommReceiveMessage_OneValidateError);
    RUN_TEST(test_CommReceiveMessage_TwoValidateError);
    RUN_TEST(test_CommReceiveMessage_FiveValidateError);
    RUN_TEST(test_CommReceiveVideo_Pass);
    RUN_TEST(test_CommReceiveVideo_FiveValidateError);
    RUN_TEST(test_CommReceiveVideo_MsgTooLong);
}

#endif