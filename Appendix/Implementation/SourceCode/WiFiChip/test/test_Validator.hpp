#ifndef _TEST_VALIDATOR_H_
#define _TEST_VALIDATOR_H_

#include <unity.h>
#include <CRC.hpp>

void test_GetValidationCode()
{
    Validator::CRC crc = Validator::CRC();
    uint16_t expected = 0x89C3;

    int len = 4;
    uint8_t message[len] = {0x01, 0x02, 0x03, 0x04};
    TEST_ASSERT_EQUAL(expected, crc.GetValdidationCode(message, &len));
}

void test_ValidateCode_Pass()
{
    Validator::CRC crc = Validator::CRC();
    
    int len = 6;
    uint8_t message[len] = {0x01, 0x02, 0x03, 0x04, 0x89, 0xc3};
    TEST_ASSERT_EQUAL(true, crc.Validate(message,&len));
}

void test_ValidateCode_Error()
{
    Validator::CRC crc = Validator::CRC();

    int len = 6;
    uint8_t message[len] = {0x01, 0x02, 0x03, 0x04, 0x00, 0x00};
    TEST_ASSERT_EQUAL(false, crc.Validate(message,&len));
}

void test_CRC()
{
    RUN_TEST(test_GetValidationCode);
    RUN_TEST(test_ValidateCode_Error);
    RUN_TEST(test_ValidateCode_Pass);
}

#endif