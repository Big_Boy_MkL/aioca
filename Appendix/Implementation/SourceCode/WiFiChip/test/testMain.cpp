#include <unity.h>
#include "test_Validator.hpp"
#include "test_BUS.hpp"
#include "test_TimeStamp.hpp"
#include "test_Communication.hpp"
#include "test_intergation_ESP32_MainController_SPI.hpp"

void setUp(void)
{
}

void tearDown(void)
{
}

extern "C" 
{
    void app_main();
}

void app_main()
{ 
    UNITY_BEGIN();

    // test_CRC();
    // test_BUS();
    // test_TimeStamp();  
    // test_COM();

    test_Intergration();

    UNITY_END(); // stop unit testing

    while (1)
    {
    }   
}