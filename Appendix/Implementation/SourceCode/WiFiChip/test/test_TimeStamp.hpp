#ifndef _TEST_TIMESTAMP_H_
#define _TEST_TIMESTAMP_H_

#include <unity.h>
#include <GlobalDefines.hpp>
#include <TimeStamp.hpp>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

void test_TimeStamp_StampValue()
{
    auto time = Utility::TimeStamp(TIMER_GROUP_1, TIMER_0);
    MessageQueueData data;
    data.Timestamp = 0;

    vTaskDelay(200/portTICK_PERIOD_MS);
    time.AddTimeStamp(&data);

    TEST_ASSERT_NOT_EQUAL(0, data.Timestamp);
}

void test_TimeStamp_CheckTimeStamp_timeout()
{
    auto time = Utility::TimeStamp(TIMER_GROUP_1, TIMER_0);
    MessageQueueData data;
    time.AddTimeStamp(&data);
    
    vTaskDelay((TIMESTAMP_MESSAGE_TIMEOUT+1)*1000/portTICK_PERIOD_MS);

    TEST_ASSERT_EQUAL(0, time.CompareTimeStamp(&data));
}


void test_TimeStamp_CheckTimeStamp_NoTimeout()
{
    auto time = Utility::TimeStamp(TIMER_GROUP_1, TIMER_0);
    MessageQueueData data;
    time.AddTimeStamp(&data);
    
    vTaskDelay(500/portTICK_PERIOD_MS);

    TEST_ASSERT_EQUAL(1, time.CompareTimeStamp(&data));
}


void test_TimeStamp()
{
    RUN_TEST(test_TimeStamp_StampValue);
    RUN_TEST(test_TimeStamp_CheckTimeStamp_timeout);
    RUN_TEST(test_TimeStamp_CheckTimeStamp_NoTimeout);
}

#endif