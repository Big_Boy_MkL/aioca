/*
 * LEDController.cpp
 *
 *  Created on: Sep 15, 2020
 *      Author: SYFO
 */

#include <LEDController.h>
#include "stm32h7xx_hal.h"
#include "stm32h7xx_hal_gpio.h"

namespace Hardware {

#define LED1_GPIO GPIOB
#define LED1_PIN GPIO_PIN_0

#define LED2_GPIO GPIOE
#define LED2_PIN GPIO_PIN_1

#define LED3_GPIO GPIOB
#define LED3_PIN GPIO_PIN_14

LEDController::LEDController() {
}

bool LEDController::initPins()
{
	// Enable GPIOCLOCKS
	__GPIOB_CLK_ENABLE();
	__GPIOE_CLK_ENABLE();


	/* LED 1 (PB0) */
	GPIO_InitTypeDef gpioInit;
	gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
	gpioInit.Pin = LED1_PIN;
	gpioInit.Speed = GPIO_SPEED_LOW;
	gpioInit.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(LED1_GPIO, &gpioInit);

	/* LED 2 (PE1) */
	gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
	gpioInit.Pin = LED2_PIN;
	gpioInit.Speed = GPIO_SPEED_LOW;
	gpioInit.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(LED2_GPIO, &gpioInit);


	/* LED 3 (PB14) */
	gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
	gpioInit.Pin = LED3_PIN;
	gpioInit.Speed = GPIO_SPEED_LOW;
	gpioInit.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(LED3_GPIO, &gpioInit);

	return true;
}

void LEDController::on(uint8_t led){
	switch(led){
		case 1:
			LED1_GPIO->ODR |= LED1_PIN;
			break;
		case 2:
			LED2_GPIO->ODR |= LED2_PIN;
			break;
		case 3:
			LED3_GPIO->ODR |= LED3_PIN;
			break;
	}
}

void LEDController::off(uint8_t led){
	switch(led){
		case 1:
			LED1_GPIO->ODR &= ~LED1_PIN;
			break;
		case 2:
			LED2_GPIO->ODR &= ~LED2_PIN;
			break;
		case 3:
			LED3_GPIO->ODR &= ~LED3_PIN;
			break;
	}
}


LEDController::~LEDController() {
	// TODO Auto-generated destructor stub
}

} /* namespace Hardware */
