/*
 * SettingsManager.cpp
 *
 *  Created on: 4 Nov 2020
 *      Author: SYFO
 */

#include <Storage/SettingsManager.h>
#include <Util/Version.h>
#include <Storage/Exceptions/SettingsManagerExceptions.h>
#include <cstring>
#include <cstdio>

namespace Storage {

constexpr char k_SettingsPath[] = "Settings";
constexpr char k_UserIDLookupPath[] = "luptab.txt";

SettingsManager::SettingsManager(IStorage& storageHandler) : storageHandler_(storageHandler) {
	// TODO Auto-generated constructor stub

}

unsigned int SettingsManager::AppendToUserIDLookup(char* userID)
{
	/* Read the number of stored settings */
	unsigned int n = storageHandler_.GetNumberOfFilesInDir(k_SettingsPath);

	/* Check if user id exists in look up table */
	unsigned int res = GetSettingsFileNumber(userID);
	if (res!=0xFF) return res;	// No error the user is simply already added

	/* Create new line to the lookup file */
	char fileBuffer[64];
	unsigned int len = sprintf(fileBuffer, "%s:%d\n",userID, n);

	if (len>0)
		storageHandler_.AppendToFile(k_UserIDLookupPath, (uint8_t*)fileBuffer, len);

	return n;
}

unsigned int SettingsManager::GetSettingsFileNumber(char* userID) noexcept
{
	uint8_t readBuffer[64];
	unsigned int count = 0;
	bool found = false;

	storageHandler_.ResetRead();

	while(storageHandler_.ReadLine(k_UserIDLookupPath, readBuffer, sizeof(readBuffer))){
		if (strncmp((char*)readBuffer, userID, k_UserIDSize) == 0){
			found = true;
			break;
		}
		count++;
	}

	storageHandler_.ResetRead();

	return found ? count : 0xFF;	// 0xFF signals not found
}


void SettingsManager::WriteSettings(char* userID, UserSettings_t settings)
{
	// TODO: UserID Must be null terminated
	/* Append the userID to the lookup table */
	unsigned int n = AppendToUserIDLookup(userID);

	char fileNameBuffer[32];
	sprintf(fileNameBuffer,"%s/%d",k_SettingsPath,n);

	// Create local copy of file content
	SettingFileUnion fileContent;
	memcpy(fileContent.settingFile.userid, userID, k_UserIDSize);
	fileContent.settingFile.userSettings = settings;
	fileContent.settingFile.version[0] = Version::kMajor;
	fileContent.settingFile.version[1] = Version::kMinor;
	fileContent.settingFile.version[2] = Version::kPatch;

	// Attempt to write settings to file
	try {
		storageHandler_.WriteToFile(fileNameBuffer, fileContent.raw, k_SettingFileSize);
	} catch (...){
		throw SettingsManagerException();	// Hmm
	}
}

UserSettings_t SettingsManager::ReadSettings(char* userID)
{
	/* Lookup settings file number */
	unsigned int n = GetSettingsFileNumber(userID);

	if (n==0xFF){
		// If settings doesn't exist make new account with factory settings
		WriteSettings(userID, k_FactorySettings);
		n = GetSettingsFileNumber(userID);			// Get new number
	}

	/* Open settings file */
	SettingFileUnion fileContent;

	/* Format filename */
	char fileNameBuffer[32];
	sprintf(fileNameBuffer,"%s/%d",k_SettingsPath,n);

	/* Read from the settings file */
	try {
		storageHandler_.ReadFromFile(fileNameBuffer, fileContent.raw, k_SettingFileSize);
		return fileContent.settingFile.userSettings;
	} catch (...){
		throw SettingsManagerException();	// Hmm
	}
}

SettingsManager::~SettingsManager() {
	// TODO Auto-generated destructor stub
}

} /* namespace Storage */
