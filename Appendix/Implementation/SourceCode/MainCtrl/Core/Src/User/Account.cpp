/*
 * Account.cpp
 *
 *  Created on: Nov 5, 2020
 *      Author: SYFO
 */

#include <User/Account.h>
#include <cstring>
#include <Thread/Mutex.hpp>

namespace User {

Account::Account(Storage::SettingsManager& setManager, osMutexId_t& mutex) : setManager_(setManager),
		mutex_(mutex)
{
	// Use factory settings initially
	userSettings_.settings = k_FactorySettings;
	memset(userID_,'0',k_UserIDSize);	// set 0s
	userID_[k_UserIDSize] = '\0';
}

void Account::UpdateSettings()
{
#ifndef TESTING
	osMutexAcquire(mutex_, osWaitForever);	// TODO: Implement scoped_lock!
	try {
		setManager_.WriteSettings(userID_, userSettings_.settings);
	} catch (...){
	}
	osMutexRelease(mutex_);
#else
	try {
		setManager_.WriteSettings(userID_, userSettings_.settings);
	} catch (...){
	}
#endif

}

void Account::SetSetting(unsigned int settingNumber, uint8_t settingVal)
{
	if (settingNumber > k_MaxSettingNumber) return;

#ifndef TESTING
	osMutexAcquire(mutex_, osWaitForever);	// TODO: Implement scoped_lock!
	userSettings_.raw[settingNumber] = settingVal;
	osMutexRelease(mutex_);
#else
	userSettings_.raw[settingNumber] = settingVal;
#endif

}

uint8_t Account::GetSetting(unsigned int settingNumber)
{
	if (settingNumber > k_MaxSettingNumber) return 0;

#ifndef TESTING
	osMutexAcquire(mutex_, osWaitForever);
	uint8_t retval = userSettings_.raw[settingNumber];
	osMutexRelease(mutex_);
#else
	uint8_t retval = userSettings_.raw[settingNumber];
#endif
	return retval;
}

UserSettings_t Account::FetchSettings(char* userID)
{
#ifndef TESTING
	try {
		userSettings_.settings = setManager_.ReadSettings(userID);
		return userSettings_.settings;
	} catch (...){
		throw;	/// rethrow
	}
#else
	try {
		userSettings_.settings = setManager_.ReadSettings(userID);
		return userSettings_.settings;
	} catch (...){
		throw;	/// rethrow
	}
#endif
}

void Account::SetUserID(char* userID)
{
	memcpy(userID_,userID,k_UserIDSize);	// TODO: Could potentially be unsafe as we have no idea what userID is pointing at
	//TODO: Make thread safe!
	try {
		FetchSettings(userID);					// Update settings based on UserID
	} catch (...){
		throw;	//rethrow
	}
}

UserSettings_t Account::GetSettings()
{
	return userSettings_.settings;
}

Account::~Account() {
}

} /* namespace User */
