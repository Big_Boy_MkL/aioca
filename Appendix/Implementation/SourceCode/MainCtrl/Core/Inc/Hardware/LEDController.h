/*
 * LEDController.h
 *
 *  Created on: Sep 15, 2020
 *      Author: SYFO
 */

#ifndef INC_HARDWARE_LEDCONTROLLER_H_
#define INC_HARDWARE_LEDCONTROLLER_H_

#include <stdint.h>

namespace Hardware {

class LEDController {
public:
	LEDController();

	bool initPins();

	void on(uint8_t led);
	void off(uint8_t led);

	virtual ~LEDController();
};

} /* namespace Hardware */

#endif /* INC_HARDWARE_LEDCONTROLLER_H_ */
