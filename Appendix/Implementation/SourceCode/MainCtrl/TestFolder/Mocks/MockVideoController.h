/*
 * MockVideoController.h
 *
 *  Created on: 17 Nov 2020
 *      Author: SYFO
 */

#ifndef MOCKS_MOCKVIDEOCONTROLLER_H_
#define MOCKS_MOCKVIDEOCONTROLLER_H_

#include <Video/IVideoController.h>
#include <Video/Types.h>
#include <stdint.h>

class MockVideoController : public Video::IVideoController {
public:
	MockVideoController(uint8_t vidAmount) : vidAmount_(vidAmount){}

	void Handle(uint8_t* src) {}
	void Initialize() {}

	void Save() {}
	uint32_t GetVideoSize(uint32_t vidIdx) {return vidAmount_;}

	uint8_t GetVideoAmount() {return vidAmount_;}
	uint32_t GetImagesAmount(uint32_t vidIdx) {return 0;}
	Video::VideoInfoFile GetVideoInformation(uint32_t videoIdx) {}

	uint32_t GetStoredImage(uint32_t videoNumber, uint32_t imageNumber, uint8_t* buffer, uint32_t bufSize){return 0;}

	virtual ~MockVideoController(){}
private:
	uint8_t vidAmount_;
};

#endif /* MOCKS_MOCKVIDEOCONTROLLER_H_ */
