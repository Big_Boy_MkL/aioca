/*
 * hardware.h
 *
 *  Created on: 17 Nov 2020
 *      Author: SYFO
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#include <main.h>
#include "fatfs.h"
#include <../../Src/AutogenMethods.h>
#include <../../Src/ISRCallbacks.h>

Util::InterruptWrapper dcmi_isr_wrapper;
Util::InterruptWrapper spiss_isr_wrapper;

CRC_HandleTypeDef hcrc;

DCMI_HandleTypeDef hdcmi;
DMA_HandleTypeDef hdma_dcmi_pssi;
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi1_rx;
DMA_HandleTypeDef hdma_spi1_tx;
DMA_HandleTypeDef hdma_spi2_rx;
DMA_HandleTypeDef hdma_spi2_tx;

JPEG_HandleTypeDef hjpeg;

UART_HandleTypeDef huart1;
TIM_HandleTypeDef htim1;

namespace Hardware{

	inline void SetUp(){
		HAL_Init();

		/* Configure the system clock */
		SystemClock_Config();
		HAL_Delay(1000);

		// Initialize auto-generated methods
		MX_GPIO_Init();
		MX_DMA_Init();
		MX_I2C1_Init();
		MX_USART1_UART_Init();
		MX_SPI1_Init();
		MX_DCMI_Init();
		MX_TIM1_Init();
		MX_CRC_Init();
		MX_SPI2_Init();
		MX_FATFS_Init();
		MX_JPEG_Init();

		// Bind the DMA to the DCMI
		hdcmi.DMA_Handle = &hdma_dcmi_pssi;
		hdma_dcmi_pssi.Parent = &hdcmi;
	}

}

void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	for(;;){}
  /* USER CODE END Error_Handler_Debug */
}

#endif /* HARDWARE_H_ */
