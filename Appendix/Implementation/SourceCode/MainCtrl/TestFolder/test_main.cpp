/*
 * test_main.cpp
 *
 *  Created on: Nov 5, 2020
 *      Author: SYFO
 */
#define TESTING

#include "hardware.h"
#include "main.h"
#include "Tests/test_commandprocessors.h"

#include <Storage/SD.h>
Storage::SD g_SDCard;	// Has to exist in the global environment for config

void setUp(){}
void tearDown(){}

int main()
{
	/* Setup hardware stuff */
	Hardware::SetUp();

	/* Do test */
	UNITY_BEGIN();
	test_commandprocessors();
	return UNITY_END();
}
