/*
 * test_commandprocessors.h
 *
 *  Created on: 17 Nov 2020
 *      Author: SYFO
 */

#ifndef TESTS_TEST_COMMANDPROCESSORS_H_
#define TESTS_TEST_COMMANDPROCESSORS_H_

#include <../../TestFolder/Mocks/MockCommunication.h>
#include <../../TestFolder/Mocks/MockVideoController.h>
#include <../../TestFolder/Mocks/MockCompressor.h>

#include <ESP32/Process.h>
#include <ESP32/CommandProcessors/SettingsProcessor.h>
#include <ESP32/CommandProcessors/VersionProcessor.h>
#include <ESP32/CommandProcessors/VideoDataProcessor.h>
#include <ESP32/CommandProcessors/VideoInfoProcessor.h>
#include <ESP32/CommandProcessors/VideoStorageProcessor.h>
#include <ESP32/CommandProcessors/UserLoginProcessor.h>

#include <Video/Types.h>
#include <Storage/SettingsTypes.h>

#include <Configuration/Configuration.h>
#include <Video/Controller.h>

#include <../../Middlewares/Third_Party/Unity/unity.h>

#include <Storage/SD.h>
extern Storage::SD g_SDCard;	// Has to exist in the global environment for config

// Globals
Compression::JPEGCompressor<174,144> g_JPEGCompressor(hjpeg);
Video::Controller<174,144,g_JPEGCompressor> g_vidCtrl(g_SDCard);

// Command processors
ESP32::SettingsProcessor setProcessor;
//ESP32::VideoDataProcessor vidDataProcessor(g_VideoCtrlMock);

/* Helper methods */
uint8_t get_number_of_videos_on_sd()
{
	// Variables
	ESP32::VideoStorageProcessor vidStorageProcessor(g_vidCtrl);

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_vidstorage_cmd;
	read_vidstorage_cmd.rw = 0xff;
	read_vidstorage_cmd.code = 0x11;

	vidStorageProcessor(CommunicatorMock,read_vidstorage_cmd);

	uint8_t readAmount = *buffer;

	return readAmount;
}

Video::VideoInfoFile get_video_info(uint8_t idx)
{
	// Retrieve number of videos
	uint8_t nVideos = get_number_of_videos_on_sd();

	// Variables
	ESP32::VideoInfoProcessor vidInfoProcessor(g_vidCtrl);

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_vidinfo_cmd;
	read_vidinfo_cmd.rw = 0xff;
	read_vidinfo_cmd.code = 0x12;

	read_vidinfo_cmd.arguments[0] = idx;
	vidInfoProcessor(CommunicatorMock,read_vidinfo_cmd);

	return *(reinterpret_cast<Video::VideoInfoFile*>(buffer));
}

/* Test methods */
void test_user_login_with_settings()
{
	// Variables
	ESP32::UserLoginProcessor userLoginProcessor;
	ESP32::SettingsProcessor settingsProcessor;

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	uint8_t dummyUserID[] = "0000000000000000000000000000";

	Command login_cmd;
	login_cmd.rw = 0;
	login_cmd.code = 0x45;
	memcpy(login_cmd.arguments, dummyUserID, sizeof(dummyUserID));

	// Try to login
	userLoginProcessor(CommunicatorMock,login_cmd);

	TEST_ASSERT(buffer[0]==k_AckCharacter);

	// Change some settings
	uint8_t setVal = 10;
	Command change_0_cmd;
	change_0_cmd.rw = 0;
	change_0_cmd.code = 0x10;
	change_0_cmd.arguments[0] = 0;
	change_0_cmd.arguments[1] = setVal;

	settingsProcessor(CommunicatorMock,change_0_cmd);

	TEST_ASSERT(buffer[0] == setVal);	// Assert that the returned value is in fact the expected setting value

	// Read the same setting
	Command read_0_cmd;
	read_0_cmd.rw = 0xff;
	read_0_cmd.code = 0x10;
	read_0_cmd.arguments[0] = 0;

	settingsProcessor(CommunicatorMock,read_0_cmd);

	TEST_ASSERT(buffer[0] == setVal);

}

void test_user_login_with_sd()
{
	// Variables
	ESP32::UserLoginProcessor userLoginProcessor;

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	uint8_t dummyUserID[] = "0000000000000000000000000000";

	Command login_cmd;
	login_cmd.rw = 0;
	login_cmd.code = 0x45;
	memcpy(login_cmd.arguments, dummyUserID, sizeof(dummyUserID));

	// Try to login
	userLoginProcessor(CommunicatorMock,login_cmd);

	// Verify ACK
	TEST_ASSERT(buffer[0]==k_AckCharacter);
}

void test_video_data_retrieval_full()
{
	// Variables
	ESP32::VideoDataProcessor vidDataProcessor(g_vidCtrl);

	// Get number of videos
	uint8_t nVideos = get_number_of_videos_on_sd();

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer,k_AckCharacter);	// Should reply with ACK

	uint32_t nVid = get_number_of_videos_on_sd();

	if (nVid<1) return;

	//  Create command
	Command retrieve_video_0_cmd;
	retrieve_video_0_cmd.rw = 0xff;
	retrieve_video_0_cmd.code = 0x14;
	retrieve_video_0_cmd.arguments[0] = 0;
	retrieve_video_0_cmd.arguments[1] = 59;

	printf("Started retrieving videos.\n");

	vidDataProcessor(CommunicatorMock, retrieve_video_0_cmd);

	TEST_ASSERT(buffer[0]==k_FinCharacter);
}

void test_video_info_processor_with_sd()
{
	// Retrieve number of videos
	uint8_t nVideos = get_number_of_videos_on_sd();

	// Variables
	ESP32::VideoInfoProcessor vidInfoProcessor(g_vidCtrl);

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_vidinfo_cmd;
	read_vidinfo_cmd.rw = 0xff;
	read_vidinfo_cmd.code = 0x12;

	for(int i = 0; i < nVideos; i++){
		read_vidinfo_cmd.arguments[0] = static_cast<uint8_t>(i);
		vidInfoProcessor(CommunicatorMock,read_vidinfo_cmd);

		Video::VideoInfoFile tmp = *(reinterpret_cast<Video::VideoInfoFile*>(buffer));
		printf("*** [SDINFO] VIDEO %d ***\n", i);
		printf("Start index: %lu\nDate:%d:%d:%d:%d:%d\nSize:%lu\n",
				tmp.startidx,
				tmp.time.year,
				tmp.time.month,
				tmp.time.day,
				tmp.time.hour,
				tmp.time.minute,
				tmp.size);
	}
}

void test_version_processor()
{
	// Variables
	ESP32::VersionProcessor versProcessor;
	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_firmware_cmd;
	read_firmware_cmd.rw = 0xff;
	read_firmware_cmd.code = 0x13;

	// Issue command to processor
	versProcessor(CommunicatorMock,read_firmware_cmd);

	char expected[] = {Version::kMajor, Version::kMinor, Version::kPatch};
	TEST_ASSERT_CHAR_ARRAY_WITHIN_MESSAGE(0,expected,buffer,3,"Version did not match!");
}

void test_video_storage_processor_primitive()
{
	// Variables
	uint8_t vidAmount = 100;
	MockVideoController VideoCtrlMock(vidAmount);
	ESP32::VideoStorageProcessor vidStorageProcessor(VideoCtrlMock);
	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_vidstorage_cmd;
	read_vidstorage_cmd.rw = 0xff;
	read_vidstorage_cmd.code = 0x11;

	vidStorageProcessor(CommunicatorMock,read_vidstorage_cmd);

	uint8_t readAmount = *buffer;

	TEST_ASSERT(readAmount == vidAmount);
}

void test_video_storage_processor_with_sd()
{
	// Variables
	ESP32::VideoStorageProcessor vidStorageProcessor(g_vidCtrl);

	uint8_t buffer[64];
	MockCommunication CommunicatorMock(buffer);

	// Create temp command
	Command read_vidstorage_cmd;
	read_vidstorage_cmd.rw = 0xff;
	read_vidstorage_cmd.code = 0x11;

	vidStorageProcessor(CommunicatorMock,read_vidstorage_cmd);

	uint8_t readAmount = *buffer;

	printf("[SDINFO] Amount of videos on SD: %d\n", readAmount);
}

inline void test_commandprocessors(){
	// Do before tests
	while(1){
		try {
			g_SDCard.Initialize();	// Initialize the SD-Card
			g_vidCtrl.Initialize(); // Initialize the Video controller
			break;
		} catch (...){
			continue;
		}
	}

	// Run tests
	printf("*** CommandProcessors test ***\n");
	RUN_TEST(test_version_processor);
	RUN_TEST(test_video_storage_processor_primitive);
	RUN_TEST(test_video_storage_processor_with_sd);
	RUN_TEST(test_video_info_processor_with_sd);
	RUN_TEST(test_user_login_with_sd);
	RUN_TEST(test_user_login_with_settings);
	RUN_TEST(test_video_data_retrieval_full);
}


#endif /* TESTS_TEST_COMMANDPROCESSORS_H_ */
