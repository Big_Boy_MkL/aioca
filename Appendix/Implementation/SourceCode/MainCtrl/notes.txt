* Når vi initialisere GPIO'er i Serielle klasser, så er de faktisk allerede initialiseret i initSERIAL
  da den bruger disse pins til opsætning af andre lower-level features. Men vi reintialisere for at få
  mere kontrol.

* Vi vil måske gerne kalde HAL_DMA_IRQHandler() på vores objekt når der sker interrupts! 
  Kræver at vi sætter de globale handlers til objektets private instans!