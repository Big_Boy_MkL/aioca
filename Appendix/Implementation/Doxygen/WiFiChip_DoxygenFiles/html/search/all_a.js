var searchData=
[
  ['senderror_21',['SendError',['../class_t_c_p_1_1_socket.html#a2ab8c6e5117c3ea0906d7e91b19f97d0',1,'TCP::Socket']]],
  ['sendmessage_22',['SendMessage',['../class_t_c_p_1_1_socket.html#a189baffa3164a820c329ad74d6ca74d8',1,'TCP::Socket']]],
  ['setapmode_23',['SetApMode',['../class_wifi_controller.html#adb8e79b04b2d011efab64853a1084599',1,'WifiController']]],
  ['setstamode_24',['SetStaMode',['../class_wifi_controller.html#a811a0e0feb036aaef03900d86326879c',1,'WifiController']]],
  ['socket_25',['Socket',['../class_t_c_p_1_1_socket.html',1,'TCP']]],
  ['spi_26',['SPI',['../class_bus_1_1_s_p_i.html',1,'Bus']]],
  ['start_27',['Start',['../class_t_c_p_1_1_socket.html#a90df08daa339df0e0819d022aa11a853',1,'TCP::Socket::Start()'],['../class_wifi_controller.html#a03b2d3986cf3f0605f6ce2dbd1b24ae1',1,'WifiController::Start()']]],
  ['stm32_28',['STM32',['../namespace_s_t_m32.html',1,'']]],
  ['stop_29',['Stop',['../class_t_c_p_1_1_socket.html#a7c9f3293463754e01d982d7091dd65bc',1,'TCP::Socket::Stop()'],['../class_wifi_controller.html#a7b0d2d016f4250ec3ff0527ece9b6016',1,'WifiController::Stop()']]]
];
