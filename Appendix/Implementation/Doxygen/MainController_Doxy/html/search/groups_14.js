var searchData=
[
  ['vrefbuf_20high_20impedance_37886',['VREFBUF High Impedance',['../group___s_y_s_c_f_g___v_r_e_f_b_u_f___high_impedance.html',1,'']]],
  ['vrefbuf_20voltage_20scale_37887',['VREFBUF Voltage Scale',['../group___s_y_s_c_f_g___v_r_e_f_b_u_f___voltage_scale.html',1,'']]],
  ['vcoroutineschedule_37888',['vCoRoutineSchedule',['../group__v_co_routine_schedule.html',1,'']]],
  ['vqueuedelete_37889',['vQueueDelete',['../group__v_queue_delete.html',1,'']]],
  ['vsemaphorecreatebinary_37890',['vSemaphoreCreateBinary',['../group__v_semaphore_create_binary.html',1,'']]],
  ['vsemaphoredelete_37891',['vSemaphoreDelete',['../group__v_semaphore_delete.html',1,'']]],
  ['vstreambufferdelete_37892',['vStreamBufferDelete',['../group__v_stream_buffer_delete.html',1,'']]],
  ['vtaskdelay_37893',['vTaskDelay',['../group__v_task_delay.html',1,'']]],
  ['vtaskdelayuntil_37894',['vTaskDelayUntil',['../group__v_task_delay_until.html',1,'']]],
  ['vtaskdelete_37895',['vTaskDelete',['../group__v_task_delete.html',1,'']]],
  ['vtaskendscheduler_37896',['vTaskEndScheduler',['../group__v_task_end_scheduler.html',1,'']]],
  ['vtaskgetinfo_37897',['vTaskGetInfo',['../group__v_task_get_info.html',1,'']]],
  ['vtaskgetruntimestats_37898',['vTaskGetRunTimeStats',['../group__v_task_get_run_time_stats.html',1,'']]],
  ['vtasklist_37899',['vTaskList',['../group__v_task_list.html',1,'']]],
  ['vtaskpriorityset_37900',['vTaskPrioritySet',['../group__v_task_priority_set.html',1,'']]],
  ['vtaskresume_37901',['vTaskResume',['../group__v_task_resume.html',1,'']]],
  ['vtaskresumefromisr_37902',['vTaskResumeFromISR',['../group__v_task_resume_from_i_s_r.html',1,'']]],
  ['vtaskstartscheduler_37903',['vTaskStartScheduler',['../group__v_task_start_scheduler.html',1,'']]],
  ['vtasksuspend_37904',['vTaskSuspend',['../group__v_task_suspend.html',1,'']]],
  ['vtasksuspendall_37905',['vTaskSuspendAll',['../group__v_task_suspend_all.html',1,'']]]
];
