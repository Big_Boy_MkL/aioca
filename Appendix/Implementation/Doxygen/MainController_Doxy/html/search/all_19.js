var searchData=
[
  ['y_5fmcu_5flut_30895',['Y_MCU_LUT',['../group___j_p_e_g___private___functions.html#ga095b9b00a89e34b87bec021c3ac1b6b9',1,'__JPEG_MCU_RGB_ConvertorTypeDef']]],
  ['ycbcr_5f420_5fblock_5fsize_30896',['YCBCR_420_BLOCK_SIZE',['../group___j_p_e_g___private___defines.html#gaf29a0fb2f0e901fd73576826bbb6cf95',1,'jpeg_utils.c']]],
  ['ycbcr_5f422_5fblock_5fsize_30897',['YCBCR_422_BLOCK_SIZE',['../group___j_p_e_g___private___defines.html#ga3adf79b214f3a41848798efe6966ff4c',1,'jpeg_utils.c']]],
  ['ycbcr_5f444_5fblock_5fsize_30898',['YCBCR_444_BLOCK_SIZE',['../group___j_p_e_g___private___defines.html#ga653a802dd785771f74275e4b694b15df',1,'jpeg_utils.c']]],
  ['year_30899',['year',['../struct_video_1_1_timestamp.html#ab7877177ed14655cd99e9e20061a3cb6',1,'Video::Timestamp::year()'],['../_types_8h.html#a7af2065789bc84419b8d5fe109be83b5',1,'year():&#160;Types.h']]],
  ['yuvimagetype_30900',['YUVImageType',['../class_camera_1_1_o_v7670.html#a4bf2a790c49e1acddf8df287f518c483',1,'Camera::OV7670']]]
];
