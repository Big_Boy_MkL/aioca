var searchData=
[
  ['vdd_5fvalue_37017',['VDD_VALUE',['../stm32h7xx__hal__conf_8h.html#aae550dad9f96d52cfce5e539adadbbb4',1,'stm32h7xx_hal_conf.h']]],
  ['vmessagebufferdelete_37018',['vMessageBufferDelete',['../message__buffer_8h.html#aa29d6c50600a7b0154e4a47e7c981d1a',1,'message_buffer.h']]],
  ['vportsvchandler_37019',['vPortSVCHandler',['../_free_r_t_o_s_config_8h.html#ad43047b3ea0a146673e30637488bf754',1,'FreeRTOSConfig.h']]],
  ['vqueueaddtoregistry_37020',['vQueueAddToRegistry',['../_free_r_t_o_s_8h.html#a3e103eedd5088f5f30bc55e55820a9e3',1,'FreeRTOS.h']]],
  ['vqueueunregisterqueue_37021',['vQueueUnregisterQueue',['../_free_r_t_o_s_8h.html#af007b655ee91c919bad9cd2798195899',1,'FreeRTOS.h']]],
  ['vsemaphoredelete_37022',['vSemaphoreDelete',['../semphr_8h.html#acd7d0eda0923d7caeeaaee9202c43eab',1,'semphr.h']]],
  ['vtaskgettaskinfo_37023',['vTaskGetTaskInfo',['../_free_r_t_o_s_8h.html#af91a84ee7fcd1c03bba33c0c86c9a493',1,'FreeRTOS.h']]]
];
