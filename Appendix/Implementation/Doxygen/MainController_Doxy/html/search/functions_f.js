var searchData=
[
  ['pctaskgetname_32526',['pcTaskGetName',['../task_8h.html#ae2ffb6a591fef808cf10353059b1c2bd',1,'pcTaskGetName(TaskHandle_t xTaskToQuery) PRIVILEGED_FUNCTION:&#160;tasks.c'],['../tasks_8c.html#a2ee63040e19e7c0cd7dbd070b8e1bca5',1,'pcTaskGetName(TaskHandle_t xTaskToQuery):&#160;tasks.c']]],
  ['pctimergetname_32527',['pcTimerGetName',['../timers_8h.html#a43c9186d441d03fc6f97b542f853bd1a',1,'timers.h']]],
  ['pendsv_5fhandler_32528',['PendSV_Handler',['../stm32h7xx__it_8cpp.html#a6303e1f258cbdc1f970ce579cc015623',1,'stm32h7xx_it.cpp']]],
  ['pixelconverter_32529',['PixelConverter',['../class_image_util_1_1_pixel_converter.html#a10e21da57238d089237d59c20ce5c9bd',1,'ImageUtil::PixelConverter']]],
  ['postinit_32530',['PostInit',['../class_thread_1_1_unique_access_guard.html#a875075f473fe9ea8436722576d0b8ecb',1,'Thread::UniqueAccessGuard']]],
  ['process_32531',['Process',['../class_camera_1_1_process.html#ae7c9d413eebf6f1a8d11f6fbe077e877',1,'Camera::Process::Process()'],['../class_e_s_p32_1_1_process.html#ad467d875d2ef0a24e634ec44d7476357',1,'ESP32::Process::Process()'],['../class_vision_1_1_process.html#a43fd44bbc2d400eb66ea2e35f038b282',1,'Vision::Process::Process()']]],
  ['pvportmalloc_32532',['pvPortMalloc',['../portable_8h.html#a237d63f90b28e0950bd86f76815cd6e3',1,'pvPortMalloc(size_t xSize) PRIVILEGED_FUNCTION:&#160;heap_4.c'],['../heap__4_8c.html#a0379669d07d76cc81e3028123113b271',1,'pvPortMalloc(size_t xWantedSize):&#160;heap_4.c']]],
  ['pvtaskincrementmutexheldcount_32533',['pvTaskIncrementMutexHeldCount',['../task_8h.html#a54430b124666d513049dcbf888e6935f',1,'task.h']]],
  ['pvtimergettimerid_32534',['pvTimerGetTimerID',['../timers_8h.html#ae20907a90360107d72283eb9099685ad',1,'timers.h']]],
  ['pxportinitialisestack_32535',['pxPortInitialiseStack',['../portable_8h.html#abb736501b227deb79f234b30eaf090e3',1,'portable.h']]]
];
