* commit 08028cad4f469173887969d083e3e6f54b816833
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Mon Dec 14 11:13:30 2020 +0100
| 
|     References and Javadoc
| 
* commit 33e52931d00af5c05f8bb7036b3cbc2bdfd3297e
| Author: Jesper <UmomGah>
| Date:   Sun Dec 13 16:47:14 2020 +0100
| 
|     Comments in code
| 
* commit fb22edde7f071018a2e26a0f8cca6fce15b94e4d
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Mon Nov 30 15:00:44 2020 +0100
| 
|     Todos :)
| 
* commit f70a14c67df0787584effa0665bb29e64a98de32
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Mon Nov 30 14:59:14 2020 +0100
| 
|     IntegrationTest v2
| 
* commit 506bedf6a6a60784bfcba836de265db81464e588
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Sun Nov 29 16:30:55 2020 +0100
| 
|     Javadoc
|   
*   commit 651f7720e23ddfbc795ec6d04b469639ddb680a0
|\  Merge: 4e1f12a 6a9e7d4
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Fri Nov 27 13:57:21 2020 +0100
| | 
| |     Merge branch 'master' of https://bitbucket.org/Big_Boy_MkL/aioca_app_dev into master
| | 
* | commit 4e1f12a5bc73e9ca34e5b1ce409d34e2fdf64d3a
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Fri Nov 27 13:57:15 2020 +0100
| | 
| |     Small Changes
| | 
| * commit 6a9e7d4b06237874f86bb3c0b8691d15ce979924
|/  Author: Jesper <UmomGah>
|   Date:   Fri Nov 27 13:53:42 2020 +0100
|   
|       Error message camera
| 
* commit 810b9a1f963159476a2888770badfed533882fe9
| Author: Jesper <UmomGah>
| Date:   Thu Nov 26 10:39:17 2020 +0100
| 
|     Change so video request can send NAK
| 
* commit 0d95f6e21c4d403edfa01a908f4c796b9eae9c35
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Nov 26 10:08:45 2020 +0100
| 
|     Added Progress Bar on Download
| 
* commit 9a74a1953baa6cfef2eff93f50560e5091209341
| Author: Jesper <UmomGah>
| Date:   Wed Nov 25 11:52:48 2020 +0100
| 
|     Added feature to view video from app
| 
* commit cc45ce0bc03bc11309e3cd370840dbda5a704953
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Tue Nov 24 15:03:29 2020 +0100
| 
|     Empty Constructors for Fragments
| 
* commit 2fb5f864e899da0ce71948c1c44e1908c6161dee
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Tue Nov 24 14:17:41 2020 +0100
| 
|     * Side of Card in VideoInfo Adapter
|   
*   commit 9f98c2f738864d734ab5627e914e477ae2b684a9
|\  Merge: 8ed7aaa 899d007
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Tue Nov 24 13:05:44 2020 +0000
| | 
| |     Merged in feature/AIOC-138-smartphone-fully-login-on-setti (pull request #9)
| |     
| |     * Close to Final Release!
| | 
| * commit 899d0078f8fdf18bd7776b317e4265e41e31688d
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Tue Nov 24 14:04:29 2020 +0100
|   
|       * Close to Final Release!
| 
* commit 8ed7aaa8a3dafd45e2da40d2f03239bcc33ee248
| Author: Jesper <UmomGah>
| Date:   Tue Nov 24 13:48:13 2020 +0100
| 
|     Move files
| 
* commit 7ee367fc69df77e624632ab0702d1f0a48845b86
| Author: Jesper <UmomGah>
| Date:   Tue Nov 24 09:47:31 2020 +0100
| 
|     Finish save video feature
| 
* commit f769248cb832e42dce7889adcf14e605853bfbf2
| Author: Jesper <UmomGah>
| Date:   Sun Nov 22 16:59:34 2020 +0100
| 
|     missing file from last commit
| 
* commit b696b715f810b8d41cc0587263bd4a7efa7e4f0b
| Author: Jesper <UmomGah>
| Date:   Sun Nov 22 16:58:31 2020 +0100
| 
|     Work on frame save and video creation
|   
*   commit 3b3bf48b8bb8f4b88b254a692af7699f0ff6e9ad
|\  Merge: da1e135 f0d7bce
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Fri Nov 20 14:29:59 2020 +0000
| | 
| |     Merged in feature/AIOC-126-gui-design-for-saved-video (pull request #8)
| |     
| |     Feature/AIOC-126 gui design for saved video
| | 
| * commit f0d7bce1193e051f534f8465c2fc0cb8c05db476
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Fri Nov 20 15:29:01 2020 +0100
| | 
| |     * RecyclerView GUI Design
| |     * Set up ViewModel to Insert to Database
| | 
| * commit 3d071af5296225a0e2637df1dc40ce137d9bfdfb
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Fri Nov 20 15:24:16 2020 +0100
|   
|       * RecyclerView GUI Design
|       * Set up ViewModel to Insert to Database
| 
* commit da1e1359557e0d522ef87fd7b2c3d38217b60e3a
| Author: Jesper <UmomGah>
| Date:   Fri Nov 20 14:36:56 2020 +0100
| 
|     Possible to read all frames from mainController
| 
* commit d3f345df7f5ea36876190de9f9bd5624976056fc
| Author: Jesper <UmomGah>
| Date:   Fri Nov 20 14:25:50 2020 +0100
| 
|     Possible to read all frames from mainController
| 
* commit b181dd9504276a4c1ce994b4778fbb72ec036ea9
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Fri Nov 20 14:10:22 2020 +0100
| 
|     * Added Room database for VideoFileInfo
| 
* commit bccb32eb81d1e246f21b5472544aad025c6f8657
| Author: Jesper <UmomGah>
| Date:   Fri Nov 20 09:56:38 2020 +0100
| 
|     Receive video from main controller feature - Not done yet
|   
*   commit c88925e27803a1c25266e04074270b03b313db66
|\  Merge: 7f01f4e 710d75b
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Thu Nov 19 13:58:54 2020 +0000
| | 
| |     Merged in feature/AIOC-120-singleton-til-udp-controller (pull request #7)
| |     
| |     * Reworked UdpController.java to Singleton Pattern
| | 
| * commit 710d75b99c6590cdba2999777639b2de0c86f748
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Thu Nov 19 14:58:12 2020 +0100
|   
|       * Reworked UdpController.java to Singleton Pattern
|   
*   commit 7f01f4efa904c86e9778a18908a04b3f87144c90
|\  Merge: fca0c56 cdb7a02
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Thu Nov 19 13:29:59 2020 +0000
| | 
| |     Merged in feature/AIOC-123-add-settings-repository (pull request #6)
| |     
| |     * Get Settings from MainCtrl on LoginFragment
| | 
| * commit cdb7a02ab1e270359084717976928c0e9b47ee06
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Thu Nov 19 14:28:34 2020 +0100
|   
|       * Get Settings from MainCtrl on LoginFragment
|       * Change Settings on MainCtrl from Smartphone
| 
* commit fca0c565b9f86633f707819e3669d830a528dc25
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Nov 19 13:47:07 2020 +0100
| 
|     * Get Settings from MainCtrl on LoginFragment
|     * Change Settings on MainCtrl from Smartphone
|   
*   commit df5970cd5b3234cbcb2bdfe320b80854554ec1e2
|\  Merge: 6f21577 2488d1b
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Wed Nov 18 14:54:52 2020 +0100
| | 
| |     Merge remote-tracking branch 'origin/master' into master
| |     
| |     # Conflicts:
| |     #       app/src/main/java/aioca/example/aiocaapplication/fragments/LoginFragment.java
| | 
* | commit 6f21577029a53593bc4be9eec9ac2af5ea63d94c
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Wed Nov 18 14:52:49 2020 +0100
| | 
| |     Formatting OmegaLUL
| | 
| * commit 2488d1b534838fe6e99017570f07516099448db0
|/  Author: Jesper <UmomGah>
|   Date:   Wed Nov 18 14:48:07 2020 +0100
|   
|       Changes to Authorization on MainCtrl
|       Inteface for Sockets
| 
* commit c003c692cc2376269ab5857e4dd1d2a8855b96c3
| Author: Jesper <UmomGah>
| Date:   Wed Nov 18 14:11:00 2020 +0100
| 
|     Changes to Authorization on MainCtrl
|     Inteface for Sockets
|   
*   commit 9755ea6478297633f3191d6af0abe24145eb0085
|\  Merge: 2a117ca 374b4e0
| | Author: Jesper <UmomGah>
| | Date:   Wed Nov 18 14:01:11 2020 +0100
| | 
| |     Merge remote-tracking branch 'remotes/origin/changes_to_controller_classes_and_authorization_on_mainctrl' into master
| |     
| |     # Conflicts:
| |     #       app/src/main/java/aioca/example/aiocaapplication/services/AiocaNetworkService.java
| | 
| * commit 374b4e0b88b446c93bf83a0f120dcf1e522c5edf
| | Author: Jesper <UmomGah>
| | Date:   Wed Nov 18 13:53:47 2020 +0100
| | 
| |     Changes to Authorization on MainCtrl
| |     Inteface for Sockets
| |   
* |   commit 2a117ca53e76c5d1ca57258655ffe0929ff1c55d
|\ \  Merge: aac84e6 c28b053
| | | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | | Date:   Wed Nov 18 12:23:54 2020 +0000
| | | 
| | |     Merged in feature/AIOC-119-implement-non-deprecated-ipform (pull request #5)
| | |     
| | |     Better Ip Grabber and Unit Test for it as well as first Unit Test using an Emulator
| | | 
| * | commit c28b0536c46a9ce59d978199edd38325fa89f780
|/ /  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| |   Date:   Wed Nov 18 13:23:06 2020 +0100
| |   
| |       Better Ip Grabber and Unit Test for it as well as first Unit Test using an Emulator
| |   
* |   commit aac84e6ed9ca66a0e4f829be4d770a1fa81027ad
|\ \  Merge: 05cc339 44491ee
| | | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | | Date:   Wed Nov 18 11:25:56 2020 +0000
| | | 
| | |     Merged in feature/AIOC-118-unit-tests-in-android (pull request #4)
| | |     
| | |     Feature/AIOC-118 unit tests in android
| | | 
| * | commit 44491eec8b0132831a0535a28c43bdbf1299b524
| | | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | | Date:   Wed Nov 18 12:24:56 2020 +0100
| | | 
| | |     Added Simple Test
| | | 
| * | commit 3d9d31d319608ffb3bce094f36614ae0cf5388a2
|/ /  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| |   Date:   Wed Nov 18 11:41:07 2020 +0100
| |   
| |       * First Commit
| | 
| * commit 3e60f5386ecc990abce0dff34c1bdc61557d3b1c
| | Author: Jesper <UmomGah>
| | Date:   Wed Nov 18 11:22:20 2020 +0100
| | 
| |     Changes to Authorization on MainCtrl
| |     Inteface for Sockets
| | 
| * commit c1d53b9f3888a0f0251debcbd01b7edffdb2b54b
|/  Author: Jesper <UmomGah>
|   Date:   Tue Nov 17 15:20:25 2020 +0100
|   
|       Changes to Authorization on MainCtrl
|       Inteface for Sockets
|   
*   commit 05cc33929b4b3d752d28c4b9388768df654ae487
|\  Merge: 3e5872c cc1ad57
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Fri Nov 13 12:34:36 2020 +0000
| | 
| |     Merged in reworked_to_keep_alive_camera (pull request #3)
| |     
| |     Reworked to keep alive camera
| | 
| * commit cc1ad574f825a523b5ff3426f175b5ccfbde8a32
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Fri Nov 13 13:33:18 2020 +0100
| | 
| |     * Force Close Sockets on WiFi Disconnect
| |     * Fixed bad connect to Service. This was android bugg using normal context use getApplicationContext on context to make it work
| | 
| * commit a9e0ec117959fe6292797325cd8cacba75e4157e
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Thu Nov 5 16:49:16 2020 +0100
| | 
| |     First iteration of refactoring the class
| | 
| * commit 89c358f6e457326e18fcb0380cebba8c75d32b71
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Thu Nov 5 12:58:44 2020 +0100
| | 
| |     * Force Close Sockets on WiFi Disconnect
| |     * Fixed bad connect to Service. This was android bugg using normal context use getApplicationContext on context to make it work
| | 
| * commit 69403df070b2a34d67bde45253b0540458f46511
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Wed Nov 4 14:48:45 2020 +0100
| | 
| |     * Force Close Sockets on WiFi Disconnect
| |     * Fixed bad connect to Service. This was android bugg using normal context use getApplicationContext on context to make it work
| | 
| * commit ab193d70967b2a105422bb99991f8ad83718a83f
| | Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| | Date:   Wed Nov 4 11:38:59 2020 +0100
| | 
| |     Keep Alive Implementation Update
| | 
| * commit ab3eb7447066589d9c975af0878106a03900f697
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Thu Oct 29 15:17:01 2020 +0100
|   
|       First Iteration on Keep Alive Implementation of Camera Video Feed
| 
* commit 3e5872c5f52059e45e898bdb3c4c674d46df2ff0
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Oct 29 10:45:46 2020 +0100
| 
|     Push to Master before changing from send stop to send keep alive
| 
* commit 06342bd7d918615fa7239c02ae95d01f1e890be1
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Oct 8 11:02:25 2020 +0200
| 
|     Small network bugs fixed
|   
*   commit 7cb4d0268255f81078756fb37a9acc1686b7e7e0
|\  Merge: 32c3975 6125795
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Thu Oct 8 08:39:30 2020 +0000
| | 
| |     Merged in feature/AIOC-82-networking-reworked-to-work-in-w (pull request #2)
| |     
| |     * Added Networking in new Login Layout
| | 
| * commit 61257952d1f851f04f2e58eeb8578e6f3973955d
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Thu Oct 8 10:37:36 2020 +0200
|   
|       * Added Networking in new Login Layout
|   
*   commit 32c39753064a6cff71ad6d3b3be4bc4cc49dbe8b
|\  Merge: 7f90820 587184f
| | Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
| | Date:   Wed Oct 7 12:56:29 2020 +0000
| | 
| |     Merged in feature/AIOC-70-firebase-implementation-on-smart (pull request #1)
| |     
| |     * Added Login and Register to Firebase Server
| | 
| * commit 587184f18cd46e8a27ced4afa0346f9a6f303f98
|/  Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
|   Date:   Wed Oct 7 14:53:04 2020 +0200
|   
|       * Added Login and Register to Firebase Server
| 
* commit 7f90820c50eaf0be894d582a8b11f64ce324687d
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Wed Oct 7 10:59:44 2020 +0200
| 
|     * Changed Package name
|     * Added Argon2Hasher if this is needed
| 
* commit 7ccee1b72631ba15d1b03e322ad52d44a4701bf5
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Fri Oct 2 12:39:03 2020 +0200
| 
|     * OnPause disconnect from Network, and Reconnect in onResume
| 
* commit 8b992586082224e8e012d07b842a4200b8ca359a
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Oct 1 14:19:16 2020 +0200
| 
|     * RxJava for simple socket send
| 
* commit 1916c73cdfa6d0ec15158c6c6256164274bb859f
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Fri Sep 25 11:28:59 2020 +0200
| 
|      * New Viewmodel/Fragment Structure
|      * Added Semi Working Network!
| 
* commit 0fddf41859f249cebcd9154fad5ed2c6d8f1e51f
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Sep 24 12:48:19 2020 +0200
| 
|      * Save before major changes to Structure, Implementing Correct ViewModels
| 
* commit 2936e1fab33de78f5b86e32ba2862939a73fea99
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Wed Sep 23 12:51:45 2020 +0200
| 
|      * Started on Network selection
| 
* commit 944f04fc25254b826ef83110674658fe6c906d9b
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Sat Sep 19 20:41:49 2020 +0200
| 
|     Made the shits fragments ;)
| 
* commit 96b22b378a7147b8b666671fabba663cac462b89
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Sat Sep 19 13:02:32 2020 +0200
| 
|     Make Login User check butter
|     Should prob be updated to Fragments instead
| 
* commit eef264b9c591427e77be7280685ee0db61366b33
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Sat Sep 19 10:45:41 2020 +0200
| 
|     * Login Activity Start
| 
* commit 1b3ea7cc5c83a547ac361cb0d49b232e5586a0bf
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Fri Sep 18 09:35:24 2020 +0200
| 
|     * Updated Build.gradle to newest versions
|     * Fixed Deprecated Drawable load
|     * Using MaterialSwitch now
| 
* commit c2c85544dbbe034720dce3eafeff40f224ff8bbe
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Thu Sep 17 11:51:11 2020 +0200
| 
|     * Threaded Room
|     
|     * Click behavior in Settings
| 
* commit b177d491fe1c11fb2d0b9d2b03e249fd544de686
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Wed Sep 16 15:26:37 2020 +0200
| 
|     * Fragments Working
|     * Added Working Room DB for Settings
|     * Made RecyclerView
|     * RoomDB using LiveData
| 
* commit 344cab65537697174cf368125c7d7327a350930e
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Tue Sep 15 15:33:38 2020 +0200
| 
|     Added Early project of AIOCA
| 
* commit 4cf891e14cd30f3d4a18aa6c764bea7d148e2791
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Tue Sep 15 11:17:40 2020 +0200
| 
|     Added App Project First commit
| 
* commit 813a35341d976cb11e8a1e862b096f1b4e909da6
| Author: Mikkel Jensen <MikkelJensen199726@hotmail.com>
| Date:   Tue Sep 15 11:17:07 2020 +0200
| 
|     gitignore update
| 
* commit 09034692c8ea42159cefb377d3dfb092738a1ce5
  Author: Mikkel Jensen <mikkeljensen199726@hotmail.com>
  Date:   Tue Sep 15 09:04:10 2020 +0000
  
      Initial commit
