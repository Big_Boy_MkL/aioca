import socket
import sys
import binascii
import numpy as np
import urllib
import cv2
import time
import threading    

# Create socket for server
localIp = socket.gethostbyname_ex(socket.gethostname())[2][2]
udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
udp.bind((localIp, 2000))

udp.settimeout(6.0)

send_data = bytes.fromhex('01ff04');
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.connect(("192.168.4.254", 2000))
tcp.send(send_data)
tcp.close()
send_data = bytes.fromhex('01fe04')

keepAliveCount = 0
frames = 0
size = 6000

state = 0
length = 0

first = time.perf_counter()
data = bytes()
# Let's send data through UDP protocol
while 1 :
    #Only receive in state 0 and 1
    if state == 0 or state == 1:
        try:
            #Receive UDP packet
            received, address = udp.recvfrom(size)
        except:
            break

    #Check packet is a start header
    if len(received) == 7 and state == 0 and received[1] == 0xff:
        #Reset data
        data = bytes()

        #Get length of frame
        length += received[2] << 24
        length += received[3] << 16
        length += received[4] << 8
        length += received[5]

        #Set state
        state = 1

    # Check if state is receiving
    elif state == 1:	
        #Append received data
        data += received

        #Check if done receiving or error happend
        if length == len(data):
            #Set state and reset length if done
            state = 2
            length = 0
        elif length < len(data):
            #Set state and reset length if error happend
            state = 0
            length = 0

    #Display frame
    elif state == 2:
        imageData = np.frombuffer(data, dtype="uint8")
        image = cv2.imdecode(np.asarray(imageData, dtype="uint8"), cv2.IMREAD_COLOR)
        if type(image) != type(None):
            cv2.imshow('',image)
            cv2.waitKey(1)
            frames += 1
            state = 0
    
    keepAliveCount += 1
    if keepAliveCount == 100:
        tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp.connect(("192.168.4.254", 2000))
        tcp.send(send_data)
        tcp.close()
        keepAliveCount = 0

    if frames == 1000:
        break

last = time.perf_counter()
last = last - first
print("Total time:              {:.2f}".format(last)) 
print("Num of received frames:  {:.2f}".format(frames))
print("FPS:                     {:.2f}".format(frames/last))

# close the socket
udp.close()
tcp.close()