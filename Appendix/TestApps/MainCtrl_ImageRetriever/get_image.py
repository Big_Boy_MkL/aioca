import serial
import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
from enum import Enum

class TransmissionMode():
    kGrayscale = 0
    kRGBFullscale = 1
    kYUVFullscale = 2

bytes_per_pixel_lookup = {
    0 : 1,
    1 : 3,
    2 : 2
}

ser = serial.Serial(port = "COM4", baudrate=512000,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)


START = b"\n"
END = b"\r"

plt.ion()

def ycbcr2rgb(im):
    xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
    rgb = im.astype(np.float)
    rgb[:,:,[1,2]] -= 128
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb > 255, 255)
    np.putmask(rgb, rgb < 0, 0)
    return np.uint8(rgb)

def YUV422ToYUV(yuv422_array):
    yuv = []
    i = 0
    while (i < len(yuv422_array)):
        u = yuv422_array[i]
        y0 = yuv422_array[i+1]
        v = yuv422_array[i+2]
        y1 = yuv422_array[i+3]
        
        yuv.append(y0)
        yuv.append(u)
        yuv.append(v)
        yuv.append(y1)
        yuv.append(u)
        yuv.append(v)

        i+=4
    return np.array(yuv)

def PrintHeader(width,height,bytes_pr_pixel,mode,size):
    print(f"**** HEADER ****\nWidth: {width}\nHeight: {height}\nBytes per pixel: {bytes_pr_pixel}\nMode: {mode}\nSize: {size}\n****************")

def RetrieveData():
    imageCount = 0

    while (True):
        if(ser.in_waiting > 0):
            # Recieve image header
            while(True):
                try:
                    width = int(ser.readline())
                    height = int(ser.readline())
                    mode = int(ser.readline())
                    break
                except:
                    pass

            bytes_pr_pixel = bytes_per_pixel_lookup[mode]

            size = width * height * bytes_pr_pixel

            PrintHeader(width,height,bytes_pr_pixel,mode,size)

            raw_data = []

            # Recieve pixels
            while (size != 0):
                y = ser.read()
                raw_data.append((int.from_bytes(y, "big")))
                size-=1

            if (mode == TransmissionMode.kRGBFullscale):
                np_data = np.array(raw_data)
                img = np_data.reshape((height,width,3))

                print("Recieved image!")
                print("Creating and saving image in RGB mode!")

                # Show image
                plt.imshow(img)
                plt.pause(0.001)
                plt.show()

            if (mode == TransmissionMode.kYUVFullscale):
                np_data = np.array(raw_data)

                yuv_data = YUV422ToYUV(np_data)

                img = yuv_data.reshape((height,width,3))

                RGBMatrix = ycbcr2rgb(img)

                print("Recieved image!")
                print("Creating and saving image in RGB mode!")

                # Show image
                plt.imshow(RGBMatrix)
                plt.pause(0.001)
                plt.show()


            if (mode == TransmissionMode.kGrayscale):
                
                np_data = np.array(raw_data)
                img = np_data.reshape((height,width))

                print("Recieved image!")
                print("Creating and saving image in grayscale mode!")

                # Show image
                plt.imshow(img,cmap="gray",vmin=0,vmax=255)
                plt.pause(0.001)
                plt.show()

            ser.reset_input_buffer()


                    
def main():
    RetrieveData()
    
# while(True):
#     if(ser.in_waiting > 0):
#         print("Serial data ready!")
#         print("Started recieving image..")
#         index = 0
#         while(index < 307200):
#             y = ser.read()
#             bindata.append((int.from_bytes(y, "big")))
#             index+=1
#         print("Done recieving image, converting and displaying!")

#         print(len(bindata))
#         data = np.array(bindata)

#         print(data)

#         try:
#             with open('dump.bin','wb+') as f:
#                 f.writelines(b''.join(bindata))
#         except:
#             pass

#         img_up = data.copy().reshape((480,640))
#         img_down = data.copy().reshape((640,480))

#         print(np.max(data))

#         plt.subplot(2,1,1)
#         plt.imshow(img_up,cmap="gray",vmin=0,vmax=255)
#         plt.subplot(2,1,2)
#         plt.imshow(img_down,cmap="gray",vmin=0,vmax=255)
#         plt.show()

            
if __name__ == "__main__":
    main()